// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkOpenFilterNode.h"

#include "dtkAbstractOpenFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkOpenFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkOpenFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     radiusRecv;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkOpenFilterNode
// /////////////////////////////////////////////////////////////////

dtkOpenFilterNode::dtkOpenFilterNode(void) : dtkComposerNodeObject<dtkAbstractOpenFilter>(), d(new dtkOpenFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::open::pluginFactory());

    this->appendReceiver(&d->radiusRecv);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkOpenFilterNode::~dtkOpenFilterNode(void)
{
    delete d;
}

void dtkOpenFilterNode::run(void)
{
    if (d->radiusRecv.isEmpty() || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

       dtkAbstractOpenFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Open filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->setRadius(d->radiusRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}


//
// dtkOpenFilterNode.cpp ends here
