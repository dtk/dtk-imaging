// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractWindowingFilter;
class dtkWindowingFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkWindowingFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkWindowingFilterNode : public dtkComposerNodeObject<dtkAbstractWindowingFilter>
{
public:
     dtkWindowingFilterNode(void);
    ~dtkWindowingFilterNode(void);

public:
    void run(void);

private:
    dtkWindowingFilterNodePrivate *d;
};

//
// dtkWindowingFilterNode.h ends here
