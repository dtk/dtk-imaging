// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractDivideFilter;
class dtkDivideFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkDivideFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkDivideFilterNode : public dtkComposerNodeObject<dtkAbstractDivideFilter>
{
public:
     dtkDivideFilterNode(void);
    ~dtkDivideFilterNode(void);

public:
    void run(void);

private:
    dtkDivideFilterNodePrivate *d;
};

//
// dtkDivideFilterNode.h ends here
