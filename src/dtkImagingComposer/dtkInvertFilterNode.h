// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractInvertFilter;
class dtkInvertFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkInvertFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkInvertFilterNode : public dtkComposerNodeObject<dtkAbstractInvertFilter>
{
public:
     dtkInvertFilterNode(void);
    ~dtkInvertFilterNode(void);

public:
    void run(void);

private:
    dtkInvertFilterNodePrivate *d;
};

//
// dtkInvertFilterNode.h ends here
