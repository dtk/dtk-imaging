// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkErosionFilterNode.h"

#include "dtkAbstractErosionFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkErosionFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkErosionFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     radius;
    dtkComposerTransmitterReceiver<dtkImage *> image_in;

    dtkComposerTransmitterEmitter<dtkImage *>  image_out;
};

// /////////////////////////////////////////////////////////////////
// dtkErosionFilterNode
// /////////////////////////////////////////////////////////////////

dtkErosionFilterNode::dtkErosionFilterNode(void) : dtkComposerNodeObject<dtkAbstractErosionFilter>(), d(new dtkErosionFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::erosion::pluginFactory());

    this->appendReceiver(&d->radius);
    this->appendReceiver(&d->image_in);

    this->appendEmitter (&d->image_out);
}

dtkErosionFilterNode::~dtkErosionFilterNode(void)
{
    delete d;
}

void dtkErosionFilterNode::run(void)
{
    if (d->radius.isEmpty() || d->image_in.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractErosionFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Erosion filter found. Aborting.";
            return;
        }
        filter->setImage(d->image_in.data());
        filter->setRadius(d->radius.constData());
        filter->run();
        d->image_out.setData(filter->filteredImage());
    }
}

//
// dtkErosionFilterNode.cpp ends here
