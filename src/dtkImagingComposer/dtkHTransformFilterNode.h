// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractHTransformFilter;

// ///////////////////////////////////////////////////////////////////
// dtkHTransformFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkHTransformFilterNode : public dtkComposerNodeObject<dtkAbstractHTransformFilter>
{
public:
     dtkHTransformFilterNode(void);
    ~dtkHTransformFilterNode(void);

public:
    void run(void);

private:
    class dtkHTransformFilterNodePrivate *d;
};

//
// dtkHTransformFilterNode.h ends here
