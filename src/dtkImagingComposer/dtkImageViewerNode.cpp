// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageViewerNode.h"

#include "dtkAbstractImageViewer.h"
#include "dtkImaging.h"
#include "dtkImage.h"
#include "dtkImageMesh.h"

#include <dtkLog>
#include <dtkWidgets>

// /////////////////////////////////////////////////////////////////
// dtkViewerNodePrivate declaration
// /////////////////////////////////////////////////////////////////

class dtkImageViewerNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage     *> img_rcv;
    dtkComposerTransmitterReceiver<dtkImageMesh *> msh_rcv;
};

// /////////////////////////////////////////////////////////////////
// dtkViewerNode implementation
// /////////////////////////////////////////////////////////////////

dtkImageViewerNode::dtkImageViewerNode(void) : dtkComposerNodeObject<dtkAbstractImageViewer>(), d(new dtkImageViewerNodePrivate())
{
    this->setFactory(dtkImaging::viewer::pluginFactory());

    this->appendReceiver(&d->img_rcv);
    this->appendReceiver(&d->msh_rcv);
}

dtkImageViewerNode::~dtkImageViewerNode(void)
{
    delete d;
}

void dtkImageViewerNode::run(void)
{
    if (d->img_rcv.isEmpty() && d->msh_rcv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "None input is set. Aborting.";
        return;

    } else {
        dtkAbstractImageViewer *viewer = this->object();
        dtkViewController::instance()->insert(viewer);

        if (!viewer) {
            dtkError() << Q_FUNC_INFO << "Viewer cannot be instanciated.";
            return;
        }
        if (!d->img_rcv.isEmpty()) {
            viewer->display(d->img_rcv.constData());
        }
        if (!d->msh_rcv.isEmpty()) {
            viewer->display(d->msh_rcv.constData());
        }
    }
}

//
// dtkViewerNode.cpp ends here
