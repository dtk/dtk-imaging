// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractMeshingFilter;

// ///////////////////////////////////////////////////////////////////
// dtkMeshingFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkMeshingFilterNode : public dtkComposerNodeObject<dtkAbstractMeshingFilter>
{
public:
     dtkMeshingFilterNode(void);
    ~dtkMeshingFilterNode(void);

public:
    void run(void);

private:
    class dtkMeshingFilterNodePrivate *d;
};

//
// dtkMeshingFilterNode.h ends here
