// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractDilateFilter;
class dtkDilateFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkDilateFilterNode : public dtkComposerNodeObject<dtkAbstractDilateFilter>
{
public:
     dtkDilateFilterNode(void);
    ~dtkDilateFilterNode(void);

public:
    void run(void);

private:
    dtkDilateFilterNodePrivate *d;
};

//
// dtkDilateFilterNode.h ends here
