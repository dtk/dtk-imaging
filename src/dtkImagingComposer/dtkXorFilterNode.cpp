// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkXorFilterNode.h"

#include "dtkAbstractXorFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkXorFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkXorFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> lhs;
    dtkComposerTransmitterReceiver<dtkImage *> rhs;

    dtkComposerTransmitterEmitter<dtkImage *>  res;
};

// /////////////////////////////////////////////////////////////////
// dtkXorFilterNode
// /////////////////////////////////////////////////////////////////

dtkXorFilterNode::dtkXorFilterNode(void) : dtkComposerNodeObject<dtkAbstractXorFilter>(), d(new dtkXorFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::xorOp::pluginFactory());

    this->appendReceiver(&d->lhs);
    this->appendReceiver(&d->rhs);

    this->appendEmitter (&d->res);

}

dtkXorFilterNode::~dtkXorFilterNode(void)
{
    delete d;
}

void dtkXorFilterNode::run(void)
{
    if (d->rhs.isEmpty() || d->lhs.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractXorFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No XOR filter found. Aborting.";
            return;
        }
        filter->setLhs(d->lhs.data());
        filter->setRhs(d->rhs.data());
        filter->run();
        d->res.setData(filter->result());
    }
}

//
// dtkXorFilterNode.cpp ends here
