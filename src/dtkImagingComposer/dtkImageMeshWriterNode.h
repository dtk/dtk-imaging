// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkImageMeshWriter;
class dtkImageMeshWriterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkImageMeshWriterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkImageMeshWriterNode : public dtkComposerNodeObject<dtkImageMeshWriter>
{
public:
     dtkImageMeshWriterNode(void);
    ~dtkImageMeshWriterNode(void);

public:
    void run(void);

private:
    dtkImageMeshWriterNodePrivate *d;
};

//
// dtkImageMeshWriterNode.h ends here
