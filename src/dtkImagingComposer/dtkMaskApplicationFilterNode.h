// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractMaskApplicationFilter;
class dtkMaskApplicationFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkMaskApplicationFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkMaskApplicationFilterNode : public dtkComposerNodeObject<dtkAbstractMaskApplicationFilter>
{
public:
     dtkMaskApplicationFilterNode(void);
    ~dtkMaskApplicationFilterNode(void);

public:
    void run(void);

private:
    dtkMaskApplicationFilterNodePrivate *d;
};

//
// dtkMaskApplicationFilterNode.h ends here
