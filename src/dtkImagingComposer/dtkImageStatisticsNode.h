// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractImageStatistics;
class dtkImageStatisticsNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkImageStatisticsNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkImageStatisticsNode : public dtkComposerNodeObject<dtkAbstractImageStatistics>
{
public:
     dtkImageStatisticsNode(void);
    ~dtkImageStatisticsNode(void);

public:
    void run(void);

private:
    dtkImageStatisticsNodePrivate *d;
};

//
// dtkImageStatisticsNode.h ends here
