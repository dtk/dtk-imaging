// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractGaussianFilter;
class dtkGaussianFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkGaussianFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkGaussianFilterNode : public dtkComposerNodeObject<dtkAbstractGaussianFilter>
{
public:
     dtkGaussianFilterNode(void);
    ~dtkGaussianFilterNode(void);

public:
    void run(void);

private:
    dtkGaussianFilterNodePrivate *d;
};

//
// dtkGaussianFilterNode.h ends here
