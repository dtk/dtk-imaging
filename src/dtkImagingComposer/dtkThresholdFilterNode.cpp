// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkThresholdFilterNode.h"

#include "dtkAbstractThresholdFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkThresholdFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkThresholdFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     threshold;
    dtkComposerTransmitterReceiver<double>     outsideValue;
    dtkComposerTransmitterReceiver<bool>       mode;
    dtkComposerTransmitterReceiver<dtkImage *> image;

    dtkComposerTransmitterEmitter<dtkImage *>  filteredImage;
};

// /////////////////////////////////////////////////////////////////
// dtkThresholdFilterNode implementation
// /////////////////////////////////////////////////////////////////

dtkThresholdFilterNode::dtkThresholdFilterNode(void) : dtkComposerNodeObject<dtkAbstractThresholdFilter>(), d(new dtkThresholdFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::threshold::pluginFactory());

    this->appendReceiver(&d->threshold);
    this->appendReceiver(&d->outsideValue);
    this->appendReceiver(&d->mode);
    this->appendReceiver(&d->image);

    this->appendEmitter (&d->filteredImage);
}

dtkThresholdFilterNode::~dtkThresholdFilterNode(void)
{
    delete d;
}

void dtkThresholdFilterNode::run(void)
{
    if (d->threshold.isEmpty() || d->outsideValue.isEmpty() || d->mode.isEmpty() || d->image.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "Missing input. Aborting.";
        return;

    } else {

        dtkAbstractThresholdFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Invert filter found. Aborting.";
            return;
        }

        filter->setThreshold(d->threshold.data());
        filter->setOutsideValue(d->outsideValue.data());
        filter->setMode(d->mode.data());
        filter->setImage(d->image.data());
        filter->run();
        d->filteredImage.setData(filter->filteredImage());
        qDebug() << "filters done";
    }
}

//
// dtkThresholdFilterNode.cpp ends here
