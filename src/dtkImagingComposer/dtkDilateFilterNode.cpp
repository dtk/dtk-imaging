// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDilateFilterNode.h"

#include "dtkAbstractDilateFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkDilateFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkDilateFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double >    radiusRecv;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkDilateFilterNode
// /////////////////////////////////////////////////////////////////

dtkDilateFilterNode::dtkDilateFilterNode(void) : dtkComposerNodeObject<dtkAbstractDilateFilter>(), d(new dtkDilateFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::dilate::pluginFactory());

    this->appendReceiver(&d->radiusRecv);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkDilateFilterNode::~dtkDilateFilterNode(void)
{
    delete d;
}

void dtkDilateFilterNode::run(void)
{
    if (d->radiusRecv.isEmpty() || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractDilateFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Dilate filter found. Aborting.";
            return;
        }

        filter->setImage(d->imgRecv.data());
        filter->setRadius(d->radiusRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkDilateFilterNode.cpp ends here
