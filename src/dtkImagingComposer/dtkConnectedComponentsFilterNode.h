// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:


#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractConnectedComponentsFilter;
class dtkConnectedComponentsFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkConnectedComponentsFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkConnectedComponentsFilterNode : public dtkComposerNodeObject<dtkAbstractConnectedComponentsFilter>
{
public:
     dtkConnectedComponentsFilterNode(void);
    ~dtkConnectedComponentsFilterNode(void);

public:
    void run(void);

private:
    dtkConnectedComponentsFilterNodePrivate *d;
};

//
// dtkConnectedComponentsFilterNode.h ends here
