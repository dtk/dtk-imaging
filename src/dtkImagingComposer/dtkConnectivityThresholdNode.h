// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractConnectivityThreshold;
class dtkConnectivityThresholdNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkConnectivityThresholdComposerNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkConnectivityThresholdNode : public dtkComposerNodeObject<dtkAbstractConnectivityThreshold>
{
public:
     dtkConnectivityThresholdNode(void);
    ~dtkConnectivityThresholdNode(void);

public:
    void run(void);

private:
    dtkConnectivityThresholdNodePrivate *d;
};

//
// dtkConnectivityThresholdNode.h ends here
