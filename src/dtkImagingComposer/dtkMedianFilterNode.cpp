// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMedianFilterNode.h"

#include "dtkAbstractMedianFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkMedianFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkMedianFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkMedianFilterNode
// /////////////////////////////////////////////////////////////////

dtkMedianFilterNode::dtkMedianFilterNode(void) : dtkComposerNodeObject<dtkAbstractMedianFilter>(), d(new dtkMedianFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::median::pluginFactory());

    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkMedianFilterNode::~dtkMedianFilterNode(void)
{
    delete d;
}

void dtkMedianFilterNode::run(void)
{
    if (d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractMedianFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Median filter found. Aborting.";
            return;
        }

        filter->setImage(d->imgRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkMedianFilterNode.cpp ends here
