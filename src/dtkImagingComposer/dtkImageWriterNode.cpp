// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageWriterNode.h"

#include "dtkImageWriter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkImageWriterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkImageWriterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<QString>    pathRecv;
    dtkComposerTransmitterReceiver<dtkImage *> dataRecv;
};

// /////////////////////////////////////////////////////////////////
// dtkImageWriterNode
// /////////////////////////////////////////////////////////////////

dtkImageWriterNode::dtkImageWriterNode(void) : dtkComposerNodeObject<dtkImageWriter>(), d(new dtkImageWriterNodePrivate())
{
    this->setFactory(dtkImaging::writer::pluginFactory());

    this->appendReceiver(&d->pathRecv);
    this->appendReceiver(&d->dataRecv);
}

dtkImageWriterNode::~dtkImageWriterNode(void)
{
    delete d;
}

void dtkImageWriterNode::run(void)
{
    if (d->pathRecv.isEmpty() || d->dataRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {
        QString path = d->pathRecv.constData();
        dtkImage *image = d->dataRecv.constData();

        if (!image) {
            dtkError() << Q_FUNC_INFO << "No image to write. Aborting.";
            return;
        }

        dtkImageWriter *writer = this->object();
        if (!writer && this->implementations().empty()) {
            dtkError() << Q_FUNC_INFO << "No Image writer found. Aborting.";
        }

        if (writer) {
            writer->setPath(path);
            writer->setImage(image);
            writer->write();

        } else {
            dtkImaging::write(path, image);
        }
    }
}

//
// dtkImageWriterNode.cpp ends here
