// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkFSLInitNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkFSLInitNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkFSLInitNode : public dtkComposerNodeLeaf
{
public:
     dtkFSLInitNode(void);
    ~dtkFSLInitNode(void);

public:
    void run(void);

private:
    dtkFSLInitNodePrivate *d;
};

//
// dtkFSLInitNode.h ends here
