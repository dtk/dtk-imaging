// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkNormalizeFilterNode.h"

#include "dtkAbstractNormalizeFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkNormalizeFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkNormalizeFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkNormalizeFilterNode
// /////////////////////////////////////////////////////////////////

dtkNormalizeFilterNode::dtkNormalizeFilterNode(void) : dtkComposerNodeObject<dtkAbstractNormalizeFilter>(), d(new dtkNormalizeFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::normalize::pluginFactory());

    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkNormalizeFilterNode::~dtkNormalizeFilterNode(void)
{
    delete d;
}

void dtkNormalizeFilterNode::run(void)
{
    if (d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractNormalizeFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Normalize filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkNormalizeFilterNode.cpp ends here
