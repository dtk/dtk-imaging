// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageMeshMappingNode.h"

#include "dtkAbstractImageMeshMapping.h"
#include "dtkImage.h"
#include "dtkImaging.h"
#include "dtkImageMesh.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkImageMeshMappingNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkImageMeshMappingNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *>     image;
    dtkComposerTransmitterReceiver<dtkImageMesh *> mesh;

    dtkComposerTransmitterEmitter<dtkImageMesh *>  resMesh;
};

// /////////////////////////////////////////////////////////////////
// dtkImageMeshMappingNode
// /////////////////////////////////////////////////////////////////

dtkImageMeshMappingNode::dtkImageMeshMappingNode(void) : dtkComposerNodeObject<dtkAbstractImageMeshMapping>(), d(new dtkImageMeshMappingNodePrivate())
{
    this->setFactory(dtkImaging::filters::imageMeshMapping::pluginFactory());

    this->appendReceiver(&d->image);
    this->appendReceiver(&d->mesh);

    this->appendEmitter (&d->resMesh);
}

dtkImageMeshMappingNode::~dtkImageMeshMappingNode(void)
{
    delete d;
}

void dtkImageMeshMappingNode::run(void)
{
    if (d->image.isEmpty() || d->mesh.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "Missing input. Aborting.";
        return;

    } else {

        dtkAbstractImageMeshMapping *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Image Mesh Mapping filter found. Aborting.";
            return;
        }
        filter->setImage(d->image.constData());
        filter->setMesh(d->mesh.data());
        filter->run();
        d->resMesh.setData(filter->resMesh());
    }
}

//
// dtkImageMeshMappingNode.cpp ends here
