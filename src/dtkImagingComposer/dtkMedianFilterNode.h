// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractMedianFilter;
class dtkMedianFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkMedianFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkMedianFilterNode : public dtkComposerNodeObject<dtkAbstractMedianFilter>
{
public:
     dtkMedianFilterNode(void);
    ~dtkMedianFilterNode(void);

public:
    void run(void);

private:
    dtkMedianFilterNodePrivate *d;
};

//
// dtkMedianFilterNode.h ends here
