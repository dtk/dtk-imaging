// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkMeshingFilterNode.h"

#include <dtkAbstractMeshingFilter.h>
#include <dtkImage.h>
#include <dtkImageMesh.h>

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkMeshingFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkMeshingFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *>    mask;
    dtkComposerTransmitterEmitter<dtkImageMesh *> mesh;
};

// /////////////////////////////////////////////////////////////////
// dtkMeshingFilterNode
// /////////////////////////////////////////////////////////////////

dtkMeshingFilterNode::dtkMeshingFilterNode(void) : dtkComposerNodeObject<dtkAbstractMeshingFilter>(), d(new dtkMeshingFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::meshing::pluginFactory());

    this->appendReceiver(&d->mask);
    this->appendEmitter (&d->mesh);
}

dtkMeshingFilterNode::~dtkMeshingFilterNode(void)
{
    delete d;
}

void dtkMeshingFilterNode::run(void)
{
    if (d->mask.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractMeshingFilter *filter = this->object();
        if(!filter) {
            dtkError() << Q_FUNC_INFO << "No Meshing filter found. Aborting.";
            return;
        }

        filter->setInput(d->mask.data());
        filter->run();
        d->mesh.setData(filter->result());
    }
}

//
// dtkMeshingFilterNode.cpp ends here
