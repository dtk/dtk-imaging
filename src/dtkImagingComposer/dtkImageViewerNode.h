// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractImageViewer;
class dtkImageViewerNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkViewerNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkImageViewerNode : public dtkComposerNodeObject<dtkAbstractImageViewer>
{
public:
     dtkImageViewerNode(void);
    ~dtkImageViewerNode(void);

public:
    void run(void);

private:
    dtkImageViewerNodePrivate *d;
};

//
// dtkViewerNode.h ends here
