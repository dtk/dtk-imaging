// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractErosionFilter;

// ///////////////////////////////////////////////////////////////////
// dtkErosionFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkErosionFilterNode : public dtkComposerNodeObject<dtkAbstractErosionFilter>
{
public:
     dtkErosionFilterNode(void);
    ~dtkErosionFilterNode(void);

public:
    void run(void);

private:
    class dtkErosionFilterNodePrivate *d;
};

//
// dtkErosionFilterNode.h ends here
