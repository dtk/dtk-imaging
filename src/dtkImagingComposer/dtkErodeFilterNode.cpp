// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkErodeFilterNode.h"

#include "dtkAbstractErodeFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkErodeFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkErodeFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     radiusRecv;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkErodeFilterNode
// /////////////////////////////////////////////////////////////////

dtkErodeFilterNode::dtkErodeFilterNode(void) : dtkComposerNodeObject<dtkAbstractErodeFilter>(), d(new dtkErodeFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::erode::pluginFactory());

    this->appendReceiver(&d->radiusRecv);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkErodeFilterNode::~dtkErodeFilterNode(void)
{
    delete d;
}

void dtkErodeFilterNode::run(void)
{
    if (d->radiusRecv.isEmpty() || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractErodeFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Erode filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->setRadius(d->radiusRecv.constData());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkErodeFilterNode.cpp ends here
