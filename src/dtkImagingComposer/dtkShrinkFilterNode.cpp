// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkShrinkFilterNode.h"

#include "dtkAbstractShrinkFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkShrinkFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkShrinkFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     xShrinkRecv;
    dtkComposerTransmitterReceiver<double>     yShrinkRecv;
    dtkComposerTransmitterReceiver<double>     zShrinkRecv;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkShrinkFilterNode
// /////////////////////////////////////////////////////////////////

dtkShrinkFilterNode::dtkShrinkFilterNode(void) : dtkComposerNodeObject<dtkAbstractShrinkFilter>(), d(new dtkShrinkFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::shrink::pluginFactory());

    this->appendReceiver(&d->xShrinkRecv);
    this->appendReceiver(&d->yShrinkRecv);
    this->appendReceiver(&d->zShrinkRecv);

    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkShrinkFilterNode::~dtkShrinkFilterNode(void)
{
    delete d;
}

void dtkShrinkFilterNode::run(void)
{
    if (d->xShrinkRecv.isEmpty() || d->yShrinkRecv.isEmpty() || d->zShrinkRecv.isEmpty() || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The inputs are not set. Aborting.";
        return;

    } else {

        dtkAbstractShrinkFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Shrink filter found. Aborting.";
            return;
        }
        filter->setXShrink(d->xShrinkRecv.data());
        filter->setYShrink(d->yShrinkRecv.data());
        filter->setZShrink(d->zShrinkRecv.data());
        filter->setImage(d->imgRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkShrinkFilterNode.cpp ends here
