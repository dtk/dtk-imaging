// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport>

#include <dtkComposer>

class dtkAbstractSegmentation;

// ///////////////////////////////////////////////////////////////////
// dtkSegmentationNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkSegmentationNode : public dtkComposerNodeObject<dtkAbstractSegmentation>
{
public:
     dtkSegmentationNode(void);
    ~dtkSegmentationNode(void);

public:
    void run(void);

private:
    class dtkSegmentationNodePrivate *d;
};

//
// dtkSegmentationNode.h ends here
