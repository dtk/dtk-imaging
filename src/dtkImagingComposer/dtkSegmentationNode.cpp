// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSegmentationNode.h"

#include <dtkAbstractSegmentation.h>
#include <dtkImage.h>
#include <dtkImagingWidgets.h>

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkSegmentationNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkSegmentationNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;
    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkSegmentationNode
// /////////////////////////////////////////////////////////////////

dtkSegmentationNode::dtkSegmentationNode(void) : dtkComposerNodeObject<dtkAbstractSegmentation>(), d(new dtkSegmentationNodePrivate())
{
    this->setFactory(dtkImaging::filters::segmentation::pluginFactory());

    this->appendReceiver(&d->imgRecv);
    this->appendEmitter (&d->imgEmt);
}

dtkSegmentationNode::~dtkSegmentationNode(void)
{
    delete d;
}

void dtkSegmentationNode::run(void)
{
    if ( d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {
        dtkAbstractSegmentation *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Segmentation filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->run();
        d->imgEmt.setData(filter->segmentedImage());
    }
}

//
// dtkSegmentationNode.cpp ends here
