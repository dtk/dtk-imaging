// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAddFilterNode.h"

#include "dtkAbstractAddFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkAddFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkAddFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     valueRecv;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkAddFilterNode
// /////////////////////////////////////////////////////////////////

dtkAddFilterNode::dtkAddFilterNode(void) : dtkComposerNodeObject<dtkAbstractAddFilter>(), d(new dtkAddFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::add::pluginFactory());

    this->appendReceiver(&d->valueRecv);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkAddFilterNode::~dtkAddFilterNode(void)
{
    delete d;
}

void dtkAddFilterNode::run(void)
{
    if (d->valueRecv.isEmpty() || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractAddFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Add filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->setValue(d->valueRecv.constData());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkAddFilterNode.cpp ends here
