// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkConnectivityThresholdNode.h"

#include "dtkAbstractConnectivityThreshold.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkConnectivityThresholdNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkConnectivityThresholdNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     minSize;
    dtkComposerTransmitterReceiver<dtkImage *> image;

    dtkComposerTransmitterEmitter<dtkImage *>  resImage;
};

// /////////////////////////////////////////////////////////////////
// dtkConnectivityThresholdNode
// /////////////////////////////////////////////////////////////////

dtkConnectivityThresholdNode::dtkConnectivityThresholdNode(void) : dtkComposerNodeObject<dtkAbstractConnectivityThreshold>(), d(new dtkConnectivityThresholdNodePrivate())
{
    this->setFactory(dtkImaging::filters::connectivityThreshold::pluginFactory());

    this->appendReceiver(&d->minSize);
    this->appendReceiver(&d->image);

    this->appendEmitter (&d->resImage);
}

dtkConnectivityThresholdNode::~dtkConnectivityThresholdNode(void)
{
    delete d;
}

void dtkConnectivityThresholdNode::run(void)
{
    if (d->minSize.isEmpty() || d->image.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "Missing input. Aborting.";
        return;

    } else {

        dtkAbstractConnectivityThreshold *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Connectivity Threshold filter found. Aborting.";
            return;
        }

        filter->setMinSize(d->minSize.constData());
        filter->setImage(d->image.data());
        filter->run();
        d->resImage.setData(filter->resImage());
    }
}

//
// dtkConnectivityThresholdNode.cpp ends here
