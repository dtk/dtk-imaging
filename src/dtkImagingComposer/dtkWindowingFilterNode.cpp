// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkWindowingFilterNode.h"

#include "dtkAbstractWindowingFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkWindowingFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkWindowingFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     minimumIntensityValueRecv;
    dtkComposerTransmitterReceiver<double>     maximumIntensityValueRecv;
    dtkComposerTransmitterReceiver<double>     minimumOutputIntensityValueRecv;
    dtkComposerTransmitterReceiver<double>     maximumOutputIntensityValue;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkWindowingFilterNode
// /////////////////////////////////////////////////////////////////

dtkWindowingFilterNode::dtkWindowingFilterNode(void) : dtkComposerNodeObject<dtkAbstractWindowingFilter>(), d(new dtkWindowingFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::windowing::pluginFactory());

    this->appendReceiver(&d->minimumIntensityValueRecv);
    this->appendReceiver(&d->maximumIntensityValueRecv);
    this->appendReceiver(&d->minimumOutputIntensityValueRecv);
    this->appendReceiver(&d->maximumOutputIntensityValue);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkWindowingFilterNode::~dtkWindowingFilterNode(void)
{
    delete d;
}

void dtkWindowingFilterNode::run(void)
{
    if (d->imgRecv.isEmpty() || d->minimumIntensityValueRecv.isEmpty()
                             || d->maximumIntensityValueRecv.isEmpty()
                             || d->minimumOutputIntensityValueRecv.isEmpty()
                             || d->maximumOutputIntensityValue.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractWindowingFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Windowing filter found. Aborting.";
            return;
        }

        filter->setMinimumIntensityValue(d->minimumIntensityValueRecv.data());
        filter->setMaximumIntensityValue(d->maximumIntensityValueRecv.data());
        filter->setMaximumOutputIntensityValue(d->minimumOutputIntensityValueRecv.data());
        filter->setMinimumOutputIntensityValue(d->maximumOutputIntensityValue.data());

        filter->setImage(d->imgRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkWindowingFilterNode.cpp ends here
