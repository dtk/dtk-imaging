/* dtkImagingComposerExtension.cpp ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) Inria.
 */
//
//


// Code:

#include "dtkImagingComposerExtensionPlugin.h"
#include "dtkImagingComposerExtension.h"
#include "dtkImaging.h"

#include <dtkComposer>
#include <dtkLog>

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

void dtkImagingComposerExtensionPlugin::initialize(void)
{
    dtkComposer::extension::pluginFactory().record("dtkImaging", dtkImagingComposerCreator);
    dtkComposerExtension *extension = dtkComposer::extension::pluginFactory().create("dtkImaging");
    bool verbose = dtkComposer::extension::pluginManager().verboseLoading();
    dtkImaging::setVerboseLoading(verbose);
    extension->extend(&(dtkComposer::node::factory()));
    if (dtkComposer::extension::isObjectManagerActive()) {
        dtkImaging::activateObjectManager();
    }
    dtkImaging::initialize();
}

void dtkImagingComposerExtensionPlugin::uninitialize(void)
{

}

// ///////////////////////////////////////////////////////////////////
// Plugin meta data
// ///////////////////////////////////////////////////////////////////

DTK_DEFINE_PLUGIN(dtkComposerExtension)

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

dtkComposerExtension *dtkImagingComposerCreator(void)
{
    return new dtkImagingComposerExtension();
}
