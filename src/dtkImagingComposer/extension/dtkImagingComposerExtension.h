// Version: $Id: d0c58a963562b8af0add85fe1984c3f775982dd8 $
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

class dtkComposerNodeFactory;

class DTKIMAGINGCOMPOSEREXTENSION_EXPORT dtkImagingComposerExtension : public dtkComposerExtension
{
public:
     dtkImagingComposerExtension(void);
    ~dtkImagingComposerExtension(void);

public:
    void extend(dtkComposerNodeFactory *factory);
};

//
// dtkImagingComposerFactoryExtension.h ends here
