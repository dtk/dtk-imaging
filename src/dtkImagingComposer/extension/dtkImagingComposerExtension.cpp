// Version: $Id: 1e79f4fa799a123ea455879ae03c89d8ac844a2f $
//
//


// Code:

#include "dtkImagingComposerExtension.h"

#include "dtkImageReaderNode.h"
#include "dtkImageWriterNode.h"
#include "dtkImageViewerNode.h"
#include "dtkImageStatisticsNode.h"

#include "dtkAddFilterNode.h"
#include "dtkCloseFilterNode.h"
#include "dtkConnectedComponentsFilterNode.h"
#include "dtkConnectivityThresholdNode.h"
#include "dtkDilateFilterNode.h"
#include "dtkDistanceMapFilterNode.h"
#include "dtkDivideFilterNode.h"
#include "dtkErodeFilterNode.h"
#include "dtkErosionFilterNode.h"
#include "dtkFSLInitNode.h"
#include "dtkGaussianFilterNode.h"
#include "dtkHTransformFilterNode.h"
#include "dtkGetEnvVariableNode.h"
#include "dtkInvertFilterNode.h"
#include "dtkMaskApplicationFilterNode.h"
#include "dtkMedianFilterNode.h"
#include "dtkMeshingFilterNode.h"
#include "dtkMeshICPNode.h"
#include "dtkMultiplyFilterNode.h"
#include "dtkNormalizeFilterNode.h"
#include "dtkOpenFilterNode.h"
#include "dtkRobexNode.h"
#include "dtkSegmentationNode.h"
#include "dtkShrinkFilterNode.h"
#include "dtkSubtractFilterNode.h"
#include "dtkSubtractionImageNode.h"
#include "dtkThresholdFilterNode.h"
#include "dtkWindowingFilterNode.h"
#include "dtkWatershedFilterNode.h"
#include "dtkXorFilterNode.h"

#include "dtkImageMeshMappingNode.h"
#include "dtkImageMeshReaderNode.h"
#include "dtkImageMeshWriterNode.h"

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

dtkImagingComposerExtension::dtkImagingComposerExtension(void) : dtkComposerExtension()
{

}

dtkImagingComposerExtension::~dtkImagingComposerExtension(void)
{

}

void dtkImagingComposerExtension::extend(dtkComposerNodeFactory *factory)
{//record the new nodes
    if (!factory) {
        dtkError() << "No composer factory, can't extend it with dtkImaging nodes ";
        return;
    }

    factory->record(":dtkComposer/dtkImageReaderNode.json"      , dtkComposerNodeCreator< dtkImageReaderNode     >);
    factory->record(":dtkComposer/dtkImageWriterNode.json"      , dtkComposerNodeCreator< dtkImageWriterNode     >);
    factory->record(":dtkComposer/dtkAddFilterNode.json"        , dtkComposerNodeCreator< dtkAddFilterNode       >);
    factory->record(":dtkComposer/dtkCloseFilterNode.json"      , dtkComposerNodeCreator< dtkCloseFilterNode     >);
    factory->record(":dtkComposer/dtkConnectedComponentsFilterNode.json", dtkComposerNodeCreator< dtkConnectedComponentsFilterNode >);
	factory->record(":dtkComposer/dtkDilateFilterNode.json"     , dtkComposerNodeCreator< dtkDilateFilterNode    >);
    factory->record(":dtkComposer/dtkDistanceMapFilterNode.json", dtkComposerNodeCreator< dtkDistanceMapFilterNode >);
    factory->record(":dtkComposer/dtkDivideFilterNode.json"     , dtkComposerNodeCreator< dtkDivideFilterNode    >);
    factory->record(":dtkComposer/dtkErodeFilterNode.json"      , dtkComposerNodeCreator< dtkErodeFilterNode     >);
    factory->record(":dtkComposer/dtkErosionFilterNode.json"    , dtkComposerNodeCreator< dtkErosionFilterNode   >);
    factory->record(":dtkComposer/dtkInvertFilterNode.json"     , dtkComposerNodeCreator< dtkInvertFilterNode    >);
    factory->record(":dtkComposer/dtkMaskApplicationFilterNode.json", dtkComposerNodeCreator< dtkMaskApplicationFilterNode >);
    factory->record(":dtkComposer/dtkMedianFilterNode.json"     , dtkComposerNodeCreator< dtkMedianFilterNode    >);
    factory->record(":dtkComposer/dtkMeshingFilterNode.json"    , dtkComposerNodeCreator< dtkMeshingFilterNode   >);
    factory->record(":dtkComposer/dtkMultiplyFilterNode.json"   , dtkComposerNodeCreator< dtkMultiplyFilterNode  >);
    factory->record(":dtkComposer/dtkNormalizeFilterNode.json"  , dtkComposerNodeCreator< dtkNormalizeFilterNode >);
    factory->record(":dtkComposer/dtkOpenFilterNode.json"       , dtkComposerNodeCreator< dtkOpenFilterNode      >);
    factory->record(":dtkComposer/dtkShrinkFilterNode.json"     , dtkComposerNodeCreator< dtkShrinkFilterNode    >);
    factory->record(":dtkComposer/dtkSubtractFilterNode.json"   , dtkComposerNodeCreator< dtkSubtractFilterNode  >);
    factory->record(":dtkComposer/dtkWindowingFilterNode.json"  , dtkComposerNodeCreator< dtkWindowingFilterNode >);
    factory->record(":dtkComposer/dtkWatershedFilterNode.json"  , dtkComposerNodeCreator< dtkWatershedFilterNode >);
    factory->record(":dtkComposer/dtkGaussianFilterNode.json"   , dtkComposerNodeCreator< dtkGaussianFilterNode  >);
    factory->record(":dtkComposer/dtkHTransformFilterNode.json" , dtkComposerNodeCreator< dtkHTransformFilterNode>);
    factory->record(":dtkComposer/dtkImageViewerNode.json"      , dtkComposerNodeCreator< dtkImageViewerNode     >);
    factory->record(":dtkComposer/dtkImageStatisticsNode.json"  , dtkComposerNodeCreator< dtkImageStatisticsNode >);
    factory->record(":dtkComposer/dtkSubtractionImageNode.json" , dtkComposerNodeCreator< dtkSubtractionImageNode>);
    factory->record(":dtkComposer/dtkSegmentationNode.json"     , dtkComposerNodeCreator< dtkSegmentationNode    >);
    factory->record(":dtkComposer/dtkGetEnvVariableNode.json"   , dtkComposerNodeCreator< dtkGetEnvVariableNode  >);
    factory->record(":dtkComposer/dtkFSLInitNode.json"          , dtkComposerNodeCreator< dtkFSLInitNode         >);
    factory->record(":dtkComposer/dtkRobexNode.json"            , dtkComposerNodeCreator< dtkRobexNode           >);
    factory->record(":dtkComposer/dtkXorFilterNode.json"        , dtkComposerNodeCreator< dtkXorFilterNode       >);
    factory->record(":dtkComposer/dtkThresholdFilterNode.json"  , dtkComposerNodeCreator< dtkThresholdFilterNode >);
    factory->record(":dtkComposer/dtkConnectivityThresholdNode.json", dtkComposerNodeCreator< dtkConnectivityThresholdNode >);
    factory->record(":dtkComposer/dtkImageMeshMappingNode.json" , dtkComposerNodeCreator< dtkImageMeshMappingNode >);
    factory->record(":dtkComposer/dtkImageMeshReaderNode.json" 	, dtkComposerNodeCreator< dtkImageMeshReaderNode >);
    factory->record(":dtkComposer/dtkImageMeshWriterNode.json" 	, dtkComposerNodeCreator< dtkImageMeshWriterNode >);
    factory->record(":dtkComposer/dtkMeshICPNode.json" 	        , dtkComposerNodeCreator< dtkMeshICPNode         >);

}

//
// dtkImagingComposerExtension.cpp ends here
