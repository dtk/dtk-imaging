/* dtkImagingComposerExtensionPlugin.h ---
 *
 * Author: Nicolas Niclausse
 * Copyright (C) Inria.
 */

#pragma once

#include <dtkImagingComposerExtensionExport.h>

#include <dtkComposer/dtkComposerExtension.h>

#include <QtCore>

class DTKIMAGINGCOMPOSEREXTENSION_EXPORT dtkImagingComposerExtensionPlugin : public dtkComposerExtensionPlugin
{
    Q_OBJECT
    Q_INTERFACES(dtkComposerExtensionPlugin)
    Q_PLUGIN_METADATA(IID "fr.inria.dtkImagingComposerExtensionPlugin" FILE "dtkImagingComposerExtensionPlugin.json")

public:
     dtkImagingComposerExtensionPlugin(void) {}
    ~dtkImagingComposerExtensionPlugin(void) {}

public:
    void   initialize(void);
    void uninitialize(void);
};

// ///////////////////////////////////////////////////////////////////
// Helper functions
// ///////////////////////////////////////////////////////////////////

DTKIMAGINGCOMPOSEREXTENSION_EXPORT dtkComposerExtension *dtkImagingComposerCreator(void);
