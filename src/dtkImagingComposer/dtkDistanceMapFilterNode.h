// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractDistanceMapFilter;
class dtkDistanceMapFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkDistanceMapFilter
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkDistanceMapFilterNode : public dtkComposerNodeObject<dtkAbstractDistanceMapFilter>
{
public:
     dtkDistanceMapFilterNode(void);
    ~dtkDistanceMapFilterNode(void);

public:
    void run(void);

private:
    dtkDistanceMapFilterNodePrivate *d;
};

//
// dtkDistanceMapFilterNode.h ends here
