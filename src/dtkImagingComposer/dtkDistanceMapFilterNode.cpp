// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDistanceMapFilterNode.h"

#include "dtkAbstractDistanceMapFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkDistanceMapFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkDistanceMapFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<dtkImage *> input;

    dtkComposerTransmitterEmitter<dtkImage *>  res;
};

// /////////////////////////////////////////////////////////////////
// dtkDistanceMapFilterNode
// /////////////////////////////////////////////////////////////////

dtkDistanceMapFilterNode::dtkDistanceMapFilterNode(void) : dtkComposerNodeObject<dtkAbstractDistanceMapFilter>(), d(new dtkDistanceMapFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::distanceMap::pluginFactory());

    this->appendReceiver(&d->input);

    this->appendEmitter(&d->res);
}

dtkDistanceMapFilterNode::~dtkDistanceMapFilterNode(void)
{
    delete d;
}

void dtkDistanceMapFilterNode::run(void)
{
    if (d->input.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractDistanceMapFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Distance Map filter found. Aborting.";
            return;
        }
        filter->setInput(d->input.data());
        filter->run();
        d->res.setData(filter->filteredImage());
    }
}

//
// dtkDistanceMapFilterNode.cpp ends here
