// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkGaussianFilterNode.h"

#include "dtkAbstractGaussianFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkGaussianFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkGaussianFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double >            sigmaRecv;
    dtkComposerTransmitterReceiver<dtkArray<double> *> sigmaValuesRecv;
    dtkComposerTransmitterReceiver<dtkImage *>         imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkGaussianFilterNode
// /////////////////////////////////////////////////////////////////

dtkGaussianFilterNode::dtkGaussianFilterNode(void) : dtkComposerNodeObject<dtkAbstractGaussianFilter>(), d(new dtkGaussianFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::gaussian::pluginFactory());

    this->appendReceiver(&d->sigmaRecv);
    this->appendReceiver(&d->sigmaValuesRecv);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkGaussianFilterNode::~dtkGaussianFilterNode(void)
{
    delete d;
}

void dtkGaussianFilterNode::run(void)
{
    if ((d->sigmaRecv.isEmpty() && d->sigmaValuesRecv.isEmpty()) || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractGaussianFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Gaussian filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());

        if (!d->sigmaRecv.isEmpty()) {
            filter->setSigma(d->sigmaRecv.data());

        } else {
            dtkArray<double> *array = d->sigmaValuesRecv.constData();
            filter->setSigmaValues(array->data());
        }
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkGaussianFilterNode.cpp ends here
