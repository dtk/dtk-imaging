// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkSubtractFilterNode.h"

#include "dtkAbstractSubtractFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkSubtractFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkSubtractFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>     radiusRecv;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkSubtractFilterNode
// /////////////////////////////////////////////////////////////////

dtkSubtractFilterNode::dtkSubtractFilterNode(void) : dtkComposerNodeObject<dtkAbstractSubtractFilter>(), d(new dtkSubtractFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::subtract::pluginFactory());

    this->appendReceiver(&d->radiusRecv);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkSubtractFilterNode::~dtkSubtractFilterNode(void)
{
    delete d;
}

void dtkSubtractFilterNode::run(void)
{
    if (d->radiusRecv.isEmpty() || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {
        dtkAbstractSubtractFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Subtract filter found. Aborting.";
            return;
        }
        filter->setImage(d->imgRecv.data());
        filter->setValue(d->radiusRecv.data());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkSubtractFilterNode.cpp ends here
