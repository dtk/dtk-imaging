// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractShrinkFilter;
class dtkShrinkFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkShrinkFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkShrinkFilterNode : public dtkComposerNodeObject<dtkAbstractShrinkFilter>
{
public:
     dtkShrinkFilterNode(void);
    ~dtkShrinkFilterNode(void);

public:
    void run(void);

private:
    dtkShrinkFilterNodePrivate *d;
};

//
// dtkShrinkFilterNode.h ends here
