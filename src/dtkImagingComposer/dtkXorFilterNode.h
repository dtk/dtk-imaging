// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractXorFilter;
class dtkXorFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkXorFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkXorFilterNode : public dtkComposerNodeObject<dtkAbstractXorFilter>
{
public:
     dtkXorFilterNode(void);
    ~dtkXorFilterNode(void);

public:
    void run(void);

private:
    dtkXorFilterNodePrivate *d;
};

//
// dtkXorFilterNode.h ends here
