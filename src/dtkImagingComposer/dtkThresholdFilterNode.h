// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractThresholdFilter;
class dtkThresholdFilterNodePrivate;

// ///////////////////////////////////////////////////////////////////
// dtkThresholdFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkThresholdFilterNode : public dtkComposerNodeObject<dtkAbstractThresholdFilter>
{
public:
     dtkThresholdFilterNode(void);
    ~dtkThresholdFilterNode(void);

public:
    void run(void);

private:
    dtkThresholdFilterNodePrivate *d;
};

//
// dtkThresholdFilterNode.h ends here
