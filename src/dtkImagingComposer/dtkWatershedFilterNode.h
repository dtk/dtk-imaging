// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingComposerExport.h>

#include <dtkComposer>

class dtkAbstractWatershedFilter;

// ///////////////////////////////////////////////////////////////////
// dtkWatershedFilterNode
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCOMPOSER_EXPORT dtkWatershedFilterNode : public dtkComposerNodeObject<dtkAbstractWatershedFilter>
{
public:
     dtkWatershedFilterNode(void);
    ~dtkWatershedFilterNode(void);

public:
    void run(void);

private:
    class dtkWatershedFilterNodePrivate *d;
};

//
// dtkWatershedFilterNode.h ends here
