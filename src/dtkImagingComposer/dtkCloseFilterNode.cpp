// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkCloseFilterNode.h"

#include "dtkAbstractCloseFilter.h"
#include "dtkImage.h"
#include "dtkImaging.h"

#include <dtkLog>

// /////////////////////////////////////////////////////////////////
// dtkCloseFilterNodePrivate
// /////////////////////////////////////////////////////////////////

class dtkCloseFilterNodePrivate
{
public:
    dtkComposerTransmitterReceiver<double>    radiusRecv;
    dtkComposerTransmitterReceiver<dtkImage *> imgRecv;

    dtkComposerTransmitterEmitter<dtkImage *>  imgEmt;
};

// /////////////////////////////////////////////////////////////////
// dtkCloseFilterNode
// /////////////////////////////////////////////////////////////////

dtkCloseFilterNode::dtkCloseFilterNode(void) : dtkComposerNodeObject<dtkAbstractCloseFilter>(), d(new dtkCloseFilterNodePrivate())
{
    this->setFactory(dtkImaging::filters::close::pluginFactory());

    this->appendReceiver(&d->radiusRecv);
    this->appendReceiver(&d->imgRecv);

    this->appendEmitter (&d->imgEmt);
}

dtkCloseFilterNode::~dtkCloseFilterNode(void)
{
    delete d;
}

void dtkCloseFilterNode::run(void)
{
    if (d->radiusRecv.isEmpty() || d->imgRecv.isEmpty()) {
        dtkError() << Q_FUNC_INFO << "The input is not set. Aborting.";
        return;

    } else {

        dtkAbstractCloseFilter *filter = this->object();
        if (!filter) {
            dtkError() << Q_FUNC_INFO << "No Close filter found. Aborting.";
            return;
        }

        filter->setImage(d->imgRecv.data());
        filter->setRadius(d->radiusRecv.constData());
        filter->run();
        d->imgEmt.setData(filter->filteredImage());
    }
}

//
// dtkCloseFilterNode.cpp ends here
