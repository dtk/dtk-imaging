// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageOperation.h"

#include "dtkImage.h"
#include "dtkImageFunction.tpp"
#include "dtkImagingTypedef.h"

#include <xtl/xvariant.hpp>

#include <xtensor/xarray.hpp>

#include <QtCore>

dtkImageFunctionBinary operator+(const dtkImage& i1, const dtkImage& i2) noexcept
{
    auto&& v1 = i1.variant();
    auto&& v2 = i2.variant();
    return xtl::visit(xtl::make_overload([&](auto& a1) -> dtkImageFunctionBinary {
                using A = std::decay_t<decltype(a1)>;
                if (auto pa2 = xtl::get_if<A>(&v2)) {
                    auto f = a1 + *pa2;
                    return dtkImageFunctionBinary(f, qMetaTypeId<typename A::value_type>());
                } else {
                    qWarning() << Q_FUNC_INFO << "Different kinds of image are trying to be added. Nothing is done.";
                    return dtkImageFunctionBinary();
                }
            }),
        v1);
}

dtkImageFunctionBinary operator-(const dtkImage& i1, const dtkImage& i2) noexcept
{
    auto&& v1 = i1.variant();
    auto&& v2 = i2.variant();
    return xtl::visit(xtl::make_overload([&](auto& a1) -> dtkImageFunctionBinary {
                using A = std::decay_t<decltype(a1)>;
                if (auto pa2 = xtl::get_if<A>(&v2)) {
                    auto f = a1 - *pa2;
                    return dtkImageFunctionBinary(f, qMetaTypeId<typename A::value_type>());
                } else {
                    qWarning() << Q_FUNC_INFO << "Different kinds of image are trying to be added. Nothing is done.";
                    return dtkImageFunctionBinary();
                }
            }),
        v1);
}

dtkImageFunctionBinary operator*(const dtkImage& i1, const dtkImage& i2) noexcept
{
    auto&& v1 = i1.variant();
    auto&& v2 = i2.variant();
    return xtl::visit(xtl::make_overload([&](auto& a1) -> dtkImageFunctionBinary {
                using A = std::decay_t<decltype(a1)>;
                if (auto pa2 = xtl::get_if<A>(&v2)) {
                    auto f = a1 * *pa2;
                    return dtkImageFunctionBinary(f, qMetaTypeId<typename A::value_type>());
                } else {
                    qWarning() << Q_FUNC_INFO << "Different kinds of image are trying to be added. Nothing is done.";
                    return dtkImageFunctionBinary();
                }
            }),
        v1);
}

dtkImageFunctionBinary operator/(const dtkImage& i1, const dtkImage& i2) noexcept
{
    auto&& v1 = i1.variant();
    auto&& v2 = i2.variant();
    return xtl::visit(xtl::make_overload([&](auto& a1) -> dtkImageFunctionBinary {
                using A = std::decay_t<decltype(a1)>;
                if (auto pa2 = xtl::get_if<A>(&v2)) {
                    auto f = a1 / *pa2;
                    return dtkImageFunctionBinary(f, qMetaTypeId<typename A::value_type>());
                } else {
                    qWarning() << Q_FUNC_INFO << "Different kinds of image are trying to be added. Nothing is done.";
                    return dtkImageFunctionBinary();
                }
            }),
        v1);
}

//
// dtkImageOperation.cpp ends here
