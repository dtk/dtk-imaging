// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport>

#include <xtl/xvariant.hpp>

#include <xtensor/xarray.hpp>

namespace dtk
{
    namespace imaging
    {
        using variant_type = xtl::variant<xt::xarray<unsigned short>,
                                          xt::xarray<short>,
                                          xt::xarray<unsigned char>,
                                          xt::xarray<char>,
                                          xt::xarray<unsigned int>,
                                          xt::xarray<int>,
                                          xt::xarray<unsigned long>,
                                          xt::xarray<long>,
                                          xt::xarray<float>,
                                          xt::xarray<double>
                                          >;

        using value_type = xtl::variant<unsigned short,
                                        short,
                                        unsigned char,
                                        char,
                                        unsigned int,
                                        int,
                                        unsigned long,
                                        long,
                                        float,
                                        double
                                        >;

        using shape_type = typename xt::xarray<double>::shape_type;
    }
}

//
// dtkImagingTypedef.h ends here
