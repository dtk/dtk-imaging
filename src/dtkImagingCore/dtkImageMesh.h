// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once
#include <dtkCore>
#include <dtkImagingCoreExport.h>

class dtkImageMeshEngine;

// /////////////////////////////////////////////////////////////////
// dtkImageMesh interface
// /////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageMesh
{
public:
    dtkImageMesh(void);
    dtkImageMesh(dtkImageMeshEngine *e);
    dtkImageMesh(const dtkImageMesh& o);

public:
    ~dtkImageMesh(void);

public:
    dtkImageMesh& operator = (const dtkImageMesh& o);

public:
    void setEngine(dtkImageMeshEngine *e);

    dtkImageMeshEngine *engine(void) const;

private:
    dtkImageMeshEngine *d;
};

DTK_DECLARE_OBJECT(dtkImageMesh*)

//
// dtkImageMesh.h ends here
