// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageData.h"

dtkImageData::dtkImageData(void) : m_array(xt::xarray<unsigned short>(shape_type({1, 1}), 0))
{

}

dtkImageData::dtkImageData(const dtkImageData& o) : m_array(o.m_array), m_pixel_type(o.m_pixel_type), transform_matrix(o.transform_matrix), metadata(o.metadata)
{
    for (int k = 0; k < 3; ++k) {
        origin[k] = o.origin[k];
        spacing[k] = o.spacing[k];
    }

}

dtkImageData *dtkImageData::clone(void) const
{
    return new dtkImageData(*this);
}

auto dtkImageData::variant(void) const -> const variant_type&
{
    return m_array;
}

auto dtkImageData::variant(void) -> variant_type&
{
    return m_array;
}

std::size_t dtkImageData::dims(void) const
{
    auto&& shape = xtl::visit(xtl::make_overload([&](auto& a) -> auto { return a.shape(); }), m_array);

    std::size_t image_dimensions = 0;
    if (shape[0] > 1) ++image_dimensions;
    if (shape[1] > 1) ++image_dimensions;
    if (shape[2] > 1) ++image_dimensions;

    return image_dimensions;
}

void *dtkImageData::rawData(void)
{
    return xtl::visit(xtl::make_overload([&](auto& a) -> void* { return a.data(); }), m_array);
}

int dtkImageData::storageType(void) const
{
    return xtl::visit(xtl::make_overload([&](auto& a) -> int
                                         {
                                             using A = std::decay_t<decltype(a)>;
                                             return qMetaTypeId<typename A::value_type>(); }), m_array);
}

auto dtkImageData::pixelType(void) const -> PixelType
{
    return m_pixel_type;
}

std::size_t dtkImageData::xDim(void) const
{
    return xtl::visit(xtl::make_overload([&](auto& a) -> auto { return a.shape()[0]; }), m_array);
}

std::size_t dtkImageData::yDim(void) const
{
    return xtl::visit(xtl::make_overload([&](auto& a) -> auto { return a.shape()[1]; }), m_array);
}

std::size_t dtkImageData::zDim(void) const
{
    return xtl::visit(xtl::make_overload([&](auto& a) -> auto { return a.shape()[2]; }), m_array);
}

std::size_t dtkImageData::numberOfChannels(void) const
{
    return xtl::visit(xtl::make_overload([&](auto& a) -> auto { return a.shape()[3]; }), m_array);
}

std::size_t dtkImageData::size(void) const
{
    return xtl::visit(xtl::make_overload([&](auto& a) -> auto { return a.size(); }), m_array);
}

//
// dtkImageData.cpp ends here
