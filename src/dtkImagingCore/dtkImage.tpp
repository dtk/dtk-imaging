// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkImageData.h"

template <typename T>
inline dtkImage::dtkImage(std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, PixelType pixel_type, T *raw_buffer) : d(new dtkImageData(x_dim, y_dim, z_dim, pixel_type, raw_buffer))
{

}

template <typename T>
inline dtkImage::dtkImage(std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, PixelType pixel_type, const T& value) : d(new dtkImageData(x_dim ,y_dim, z_dim, pixel_type, value))
{
}

template <class E>
inline dtkImage::dtkImage(const xt::xexpression<E>& e) : dtkImageSemantic<dtkImage>(), d(new dtkImageData)
{
    dtkImageSemantic<dtkImage>::assign(e);
}

template <class E>
inline dtkImage& dtkImage::operator=(const xt::xexpression<E>& e)
{
    dtkImage i(e);
    *this = std::move(i);
    return *this;
}


//
// dtkImage.tpp ends here
