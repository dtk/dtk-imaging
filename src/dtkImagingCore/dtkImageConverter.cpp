// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageConverter.h"
#include "dtkImaging.h"

namespace dtkImaging {
    DTK_DEFINE_CONCEPT(dtkImageConverter, converter, dtkImaging)
}

//
// dtkImageConverter.cpp ends here
