// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport>

#include "dtkImagingTypedef.h"

#include <xtensor/xexpression.hpp>

class dtkImageFunction;

namespace dtk
{
    // Scalar operations

    template <class E>
    void image_scalar_add_assign(imaging::variant_type& v, const E& e);

    template <class E>
    void image_scalar_sub_assign(imaging::variant_type& v, const E& e);

    template <class E>
    void image_scalar_mul_assign(imaging::variant_type& v, const E& e);

    template <class E>
    void image_scalar_div_assign(imaging::variant_type& v, const E& e);

    template <class E>
    void image_scalar_modulus_assign(imaging::variant_type& v, const E& e);

    template <class E>
    void image_scalar_bitand_assign(imaging::variant_type& v, const E& e);

    template <class E>
    void image_scalar_bitor_assign(imaging::variant_type& v, const E& e);

    template <class E>
    void image_scalar_bitxor_assign(imaging::variant_type& v, const E& e);

    // Operations involving xexpression

    template <class E>
    void image_assign(imaging::variant_type& v, const xt::xexpression<E>& e);

    template <class E>
    void image_add_assign(imaging::variant_type& v, const xt::xexpression<E>& e);

    template <class E>
    void image_sub_assign(imaging::variant_type& v, const xt::xexpression<E>& e);

    template <class E>
    void image_mul_assign(imaging::variant_type& v, const xt::xexpression<E>& e);

    template <class E>
    void image_div_assign(imaging::variant_type& v, const xt::xexpression<E>& e);

    template <class E>
    void image_modulus_assign(imaging::variant_type& v, const xt::xexpression<E>& e);

    template <class E>
    void image_bitand_assign(imaging::variant_type& v, const xt::xexpression<E>& e);

    template <class E>
    void image_bitor_assign(imaging::variant_type& v, const xt::xexpression<E>& e);

    template <class E>
    void image_bitxor_assign(imaging::variant_type& v, const xt::xexpression<E>& e);

    // Operations involving dtkImageFunction

    DTKIMAGINGCORE_EXPORT void image_function_assign(imaging::variant_type& v, const dtkImageFunction& f);
}

//
// dtkImageAssign.h ends here
