// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageMeshEngine.h"

#include "dtkImaging.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DEFINE_CONCEPT(dtkImageMeshEngine, meshEngine, dtkImaging);
}

//
// dtkImageMeshEngine.cpp ends here
