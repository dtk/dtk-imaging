// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageMesh.h"
#include "dtkImageMeshEngine.h"

// /////////////////////////////////////////////////////////////////
// dtkImageMesh implementation
// /////////////////////////////////////////////////////////////////

dtkImageMesh::dtkImageMesh(void) : d(nullptr)
{

}

dtkImageMesh::dtkImageMesh(dtkImageMeshEngine *e) : d(e)
{

}

dtkImageMesh::dtkImageMesh(const dtkImageMesh& o) : d(o.d ? o.d->clone() : nullptr)
{

}

dtkImageMesh::~dtkImageMesh(void)
{
    if (d) {
        delete d;
    }
}

dtkImageMesh& dtkImageMesh::operator = (const dtkImageMesh& o)
{
    if (this != &o) {
        if (d && d != o.d) {
            delete d;
        }
        if (o.d) {
            d = o.d->clone();
        } else {
            d = nullptr;
        }
    }

    return *this;
}

void dtkImageMesh::setEngine(dtkImageMeshEngine *e)
{
    if (d && d != e) {
        delete d;
    }
    d = e;
}

dtkImageMeshEngine *dtkImageMesh::engine(void) const
{
    return d;
}

//
// dtkImageMesh.cpp ends here
