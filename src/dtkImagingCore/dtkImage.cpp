// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImage.h"
#include "dtkImageData.h"
#include "dtkImageSemantic.tpp"

#include <dtkLog>

dtkImage::dtkImage(void) : d(new dtkImageData)
{

}

dtkImage::dtkImage(const dtkImage& o) : d(o.d->clone())
{

}

dtkImage& dtkImage::operator=(const dtkImage& o)
{
    if (this != &o) {
        if (d) {
            delete d;
        }
        d = o.d->clone();
    }
    return *this;
}

dtkImage::dtkImage(dtkImage&& o) : d(o.d)
{
    o.d = nullptr;
}

dtkImage& dtkImage::operator=(dtkImage&& o)
{
    std::swap(d, o.d);
    return *this;
}

dtkImage::~dtkImage(void)
{
    if (d) {
        delete d;
        d = nullptr;
    }
}

dtkImage& dtkImage::operator=(const dtkImageFunction& f)
{
    return dtkImageSemantic<dtkImage>::assign(f);
}

void *dtkImage::rawData(void)
{
    return d->rawData();
}

auto dtkImage::variant(void) const -> const variant_type&
{
    return d->variant();
}

auto dtkImage::variant(void) -> variant_type&
{
    return d->variant();
}

std::size_t dtkImage::dims(void) const
{
    return d->dims();
}

int dtkImage::storageType(void) const
{
    return d->storageType();
}

auto dtkImage::pixelType(void) const -> PixelType
{
    return d->pixelType();
}

std::size_t dtkImage::xDim(void) const
{
    return d->xDim();
}

std::size_t dtkImage::yDim(void) const
{
    return d->yDim();
}

std::size_t dtkImage::zDim(void) const
{
    return d->zDim();
}

std::size_t dtkImage::numberOfChannels(void) const
{
    return d->numberOfChannels();
}

std::size_t dtkImage::size(void) const
{
    return d->size();
}

const double *dtkImage::origin(void) const
{
    return d->origin;
}

const double *dtkImage::spacing(void) const
{
    return d->spacing;
}

const xt::xarray<double>& dtkImage::transformMatrix(void) const
{
    return d->transform_matrix;
}

void dtkImage::setOrigin(const double *o)
{
    d->origin[0] = o[0];
    d->origin[1] = o[1];
    d->origin[2] = o[2];
}

void dtkImage::setSpacing(const double *s)
{
    d->spacing[0] = s[0];
    d->spacing[1] = s[1];
    d->spacing[2] = s[2];
}

void dtkImage::setTransformMatrix(const double *tm)
{
    auto N = d->dims() + 1;
    for (std::size_t i = 0; i < N; i++) {
        for (std::size_t j = 0; j < N; j++) {
            d->transform_matrix(i, j) = tm[i*N+j];
        }
    }
}


dtkImage *dtkImage::fromRawData(int storage_type, std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, void * raw_data, PixelType pixel_type)
{

    dtkImage *img;

    switch (storage_type) {
    case QMetaType::Char:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<char *>(raw_data));
        break;

    case QMetaType::UChar:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<unsigned char *>(raw_data));
        break;

    case QMetaType::Short:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<short *>(raw_data));
        break;

    case QMetaType::UShort:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<unsigned short *>(raw_data));
        break;

    case QMetaType::Int:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<int *>(raw_data));
        break;

    case QMetaType::UInt:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<unsigned int *>(raw_data));
        break;

    case QMetaType::Long:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<long *>(raw_data));
        break;

    case QMetaType::ULong:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<unsigned long *>(raw_data));
        break;

    case QMetaType::Float:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<float *>(raw_data));
        break;

    case QMetaType::Double:
        img = new dtkImage(x_dim, y_dim, z_dim, pixel_type, reinterpret_cast<double *>(raw_data));
        break;

    case QMetaType::UnknownType:
    default:
        dtkWarn() << Q_FUNC_INFO << "Pixel type " << storage_type << "is not handled. Nothing is done";
        img = nullptr;
        break;
    }

    if (!img) {
        dtkWarn() << Q_FUNC_INFO << "No image instanciated. Nullptr is returned.";
    }
    return img;
}

//
// dtkImage.cpp ends here
