// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport.h>

#include <dtkCore>

class dtkImageMesh;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageMeshReader
{
public:
    virtual ~dtkImageMeshReader(void) {}

public:
    virtual dtkImageMesh *read(const QString& path) = 0;
};

// /////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkImageMeshReader *)
DTK_DECLARE_PLUGIN        (dtkImageMeshReader, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkImageMeshReader, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkImageMeshReader, DTKIMAGINGCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DECLARE_CONCEPT(dtkImageMeshReader, DTKIMAGINGCORE_EXPORT, meshReader);
}

//
// dtkImageMeshReader.h ends here
