// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport>

#include "dtkImageFunction.h"

template <class D>
class dtkImageSemantic
{
public:
    using derived_type = D;
    using self_type = dtkImageSemantic<derived_type>;

public:
    template <class E>
    dtk::disable_image_function<E, derived_type&> operator+=(const E&);

    template <class E>
    dtk::disable_image_function<E, derived_type&> operator-=(const E&);

    template <class E>
    dtk::disable_image_function<E, derived_type&> operator*=(const E&);

    template <class E>
    dtk::disable_image_function<E, derived_type&> operator/=(const E&);

    template <class E>
    dtk::disable_image_function<E, derived_type&> operator%=(const E&);

    template <class E>
    dtk::disable_image_function<E, derived_type&> operator&=(const E&);

    template <class E>
    dtk::disable_image_function<E, derived_type&> operator|=(const E&);

    template <class E>
    dtk::disable_image_function<E, derived_type&> operator^=(const E&);

    template <class E>
    derived_type& operator+=(const xt::xexpression<E>&);

    template <class E>
    derived_type& operator-=(const xt::xexpression<E>&);

    template <class E>
    derived_type& operator*=(const xt::xexpression<E>&);

    template <class E>
    derived_type& operator/=(const xt::xexpression<E>&);

    template <class E>
    derived_type& operator%=(const xt::xexpression<E>&);

    template <class E>
    derived_type& operator&=(const xt::xexpression<E>&);

    template <class E>
    derived_type& operator|=(const xt::xexpression<E>&);

    template <class E>
    derived_type& operator^=(const xt::xexpression<E>&);

    template <class E>
    derived_type& assign(const xt::xexpression<E>&);

    derived_type& assign(const dtkImageFunction&);

protected:
     dtkImageSemantic(void) = default;
    ~dtkImageSemantic(void) = default;

    dtkImageSemantic(const dtkImageSemantic&) = default;
    dtkImageSemantic& operator=(const dtkImageSemantic&) = default;

    dtkImageSemantic(dtkImageSemantic&&) = default;
    dtkImageSemantic& operator=(dtkImageSemantic&&) = default;

    derived_type& derived_cast(void) & noexcept;
    const derived_type& derived_cast(void) const & noexcept;
    derived_type derived_cast(void) && noexcept;
};

//
// dtkImageSemantic.h ends here
