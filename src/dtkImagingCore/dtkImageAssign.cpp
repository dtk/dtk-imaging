// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageAssign.h"

#include "dtkImageFunction.h"

#include <xtl/xvariant.hpp>

namespace dtk
{
    void image_function_assign(imaging::variant_type& v, const dtkImageFunction& f)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { f.assign_into(a); }), v);
    }
}

//
// dtkImageAssign.cpp ends here
