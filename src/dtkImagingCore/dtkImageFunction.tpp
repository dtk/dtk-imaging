// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkImageFunction.h"

#include <xtensor/xarray.hpp>

// ///////////////////////////////////////////////////////////////

template <class Func>
inline dtkImageFunctionUnary::dtkImageFunctionUnary(const Func& f, const int value_type) : dtkImageFunction()
{
    d = new dtkImageFunctionDataImpl<Func>(f);
    d->m_value_type = value_type;
}

// ///////////////////////////////////////////////////////////////

inline dtkImageFunctionBinary::dtkImageFunctionBinary(void)
{
    d = nullptr;
}

template <class Func>
inline dtkImageFunctionBinary::dtkImageFunctionBinary(const Func& f, const int value_type) : dtkImageFunction()
{
    d = new dtkImageFunctionDataImpl<Func>(f);
    d->m_value_type = value_type;
}

// ///////////////////////////////////////////////////////////////

template <class Func>
inline dtkImageFunctionDataImpl<Func>::dtkImageFunctionDataImpl(const function_type& func) : m_function(func)
{

}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<unsigned short>& a) const
{
    a = m_function;
}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<short>& a) const
{
    a = m_function;
}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<unsigned char>& a) const
{
    a = m_function;
}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<char>& a) const
{
    a = m_function;
}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<unsigned int>& a) const
{
    a = m_function;
}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<int>& a) const
{
    a = m_function;
}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<unsigned long>& a) const
{
    a = m_function;
}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<long>& a) const
{
    a = m_function;
}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<float>& a) const
{
    a = m_function;
}

template <class Func>
inline void dtkImageFunctionDataImpl<Func>::assign_into(xt::xarray<double>& a) const
{
    a = m_function;
}

//
// dtkImageFunction.tpp ends here
