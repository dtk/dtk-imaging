// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageMeshWriter.h"

#include "dtkImaging.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DEFINE_CONCEPT(dtkImageMeshWriter, meshWriter, dtkImaging);
}

//
// dtkImageMeshWriter.cpp ends here
