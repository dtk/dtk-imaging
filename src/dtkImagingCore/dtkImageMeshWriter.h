// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport.h>

#include <dtkCore>

class dtkImageMesh;

// /////////////////////////////////////////////////////////////////
// dtkImageMeshWriter
// /////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageMeshWriter
{
public:
    virtual ~dtkImageMeshWriter(void) {}

public:
    virtual void setPath(const QString& path) = 0;
    virtual void setMesh(dtkImageMesh *mesh) = 0;

public:
    virtual void write(void) = 0;
};

// /////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(        dtkImageMeshWriter *)
DTK_DECLARE_PLUGIN(        dtkImageMeshWriter, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkImageMeshWriter, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkImageMeshWriter, DTKIMAGINGCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DECLARE_CONCEPT(dtkImageMeshWriter, DTKIMAGINGCORE_EXPORT, meshWriter);
}

//
// dtkImageMeshWriter.h ends here
