// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

#include <dtkCore>

#include <dtkImagingCoreExport.h>

class dtkImage;

class DTKIMAGINGCORE_EXPORT dtkImageConverter
{
public:
    virtual ~dtkImageConverter(void) = default;

public:
    virtual void setInput(dtkImage *) = 0;

public:
    virtual void *output(void) = 0;

public:
    virtual int convert(void) = 0;
};

DTK_DECLARE_OBJECT(        dtkImageConverter *)
DTK_DECLARE_PLUGIN(        dtkImageConverter, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkImageConverter, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkImageConverter, DTKIMAGINGCORE_EXPORT)

namespace dtkImaging {
    DTK_DECLARE_CONCEPT(dtkImageConverter, DTKIMAGINGCORE_EXPORT, converter);
}

//
// dtkImageConverter.h ends here
