// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport>

#include "dtkImageSemantic.h"
#include "dtkImagingPixel.h"
#include "dtkImagingTypedef.h"

#include <xtensor/xexpression.hpp>

class dtkImageFunction;

class DTKIMAGINGCORE_EXPORT dtkImage : public dtkImageSemantic<dtkImage>
{
public:
    using variant_type = dtk::imaging::variant_type;
    using PixelType = dtk::imaging::PixelType;

    dtkImage(void);
    template <typename T>
    dtkImage(std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, PixelType pixel_type, T *raw_buffer);
    template <typename T>
    dtkImage(std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, PixelType pixel_type, const T& value);

    ~dtkImage(void);

    dtkImage(const dtkImage&);
    dtkImage& operator = (const dtkImage&);

    dtkImage(dtkImage&&);
    dtkImage& operator = (dtkImage&&);

    template <class E> dtkImage(const xt::xexpression<E>& e);

    template <class E> dtkImage& operator=(const xt::xexpression<E>& e);

    dtkImage& operator = (const dtkImageFunction& f);

    static dtkImage *fromRawData(int storage_type, std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, void * raw_data, PixelType pixel_type = PixelType::Scalar);
    void *rawData(void);

    const variant_type& variant(void) const;
          variant_type& variant(void);

    std::size_t dims(void) const;
    int storageType(void) const;
    PixelType pixelType(void) const;

    std::size_t xDim(void) const;
    std::size_t yDim(void) const;
    std::size_t zDim(void) const;

    std::size_t numberOfChannels(void) const;

    std::size_t size(void) const;

    const double *origin(void) const;
    const double *spacing(void) const;
    const xt::xarray<double>& transformMatrix(void) const;

    void setOrigin(const double *);
    void setSpacing(const double *);
    void setTransformMatrix(const double *);

protected:
    class dtkImageData *d;
};

Q_DECLARE_METATYPE(dtkImage);
Q_DECLARE_METATYPE(dtkImage*);

#include "dtkImage.tpp"

//
// dtkImage.h ends here
