// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAbstractImageStatistics.h"

#include "dtkImaging.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DEFINE_CONCEPT(dtkAbstractImageStatistics, statistics, dtkImaging);
}

//
// dtkAbstractImageStatistics.cpp ends here
