// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport>

#include "dtkImagingTypedef.h"

#include <xtensor/xarray.hpp>
#include <xtensor/xfunction.hpp>

#include <QtCore>

class DTKIMAGINGCORE_EXPORT dtkImageFunction
{
public:
    virtual ~dtkImageFunction(void);

    dtkImageFunction(dtkImageFunction&&);
    dtkImageFunction& operator=(dtkImageFunction&&);

    int value_type(void) const;

    void assign_into(xt::xarray<unsigned short>&) const;
    void assign_into(xt::xarray<short>&) const;
    void assign_into(xt::xarray<unsigned char>&) const;
    void assign_into(xt::xarray<char>&) const;
    void assign_into(xt::xarray<unsigned int>&) const;
    void assign_into(xt::xarray<int>&) const;
    void assign_into(xt::xarray<unsigned long>&) const;
    void assign_into(xt::xarray<long>&) const;
    void assign_into(xt::xarray<float>&) const;
    void assign_into(xt::xarray<double>&) const;

protected:
    dtkImageFunction(void) = default;

    dtkImageFunction(const dtkImageFunction&) = default;
    dtkImageFunction& operator=(const dtkImageFunction&) = default;

    class dtkImageFunctionData *d = nullptr;
};

namespace dtk
{
    namespace detail
    {
        template <class E>
        struct is_image_function_impl : std::is_base_of<dtkImageFunction, std::decay_t<E>>
        {
        };

        template <>
        struct is_image_function_impl<dtkImageFunction> : std::true_type
        {
        };
    }
    template <class E>
    using is_image_function = detail::is_image_function_impl<E>;

    template <class E, class R = void>
    using enable_image_function = typename std::enable_if<is_image_function<E>::value, R>::type;

    template <class E, class R = void>
    using disable_image_function = typename std::enable_if<!is_image_function<E>::value, R>::type;

    template <class... E>
    using has_image_function = xtl::disjunction<is_image_function<E>...>;
}

// ///////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageFunctionUnary : public dtkImageFunction
{
public:
    template <class Func> dtkImageFunctionUnary(const Func& f, const int value_type);

    dtkImageFunctionUnary(void) = default;
    ~dtkImageFunctionUnary(void) = default;

    dtkImageFunctionUnary(const dtkImageFunctionUnary&) = default;
    dtkImageFunctionUnary& operator=(const dtkImageFunctionUnary&) = default;

    dtkImageFunctionUnary(dtkImageFunctionUnary&&) = default;
    dtkImageFunctionUnary& operator=(dtkImageFunctionUnary&&) = default;
};

// ///////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageFunctionBinary : public dtkImageFunction
{
public:
    template <class Func> dtkImageFunctionBinary(const Func& f, const int value_type);

    dtkImageFunctionBinary(void);
    ~dtkImageFunctionBinary(void) = default;

    dtkImageFunctionBinary(const dtkImageFunctionBinary&) = default;
    dtkImageFunctionBinary& operator=(const dtkImageFunctionBinary&) = default;

    dtkImageFunctionBinary(dtkImageFunctionBinary&&) = default;
    dtkImageFunctionBinary& operator=(dtkImageFunctionBinary&&) = default;
};

// ///////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageFunctionComposite : public dtkImageFunction
{
public:
    //template <class Func, class R>
    //dtkImageFunctionComposite(Func&& f, R);

protected:
    dtkImageFunctionComposite(void) = default;
    ~dtkImageFunctionComposite(void) = default;

    dtkImageFunctionComposite(const dtkImageFunctionComposite&) = default;
    dtkImageFunctionComposite& operator=(const dtkImageFunctionComposite&) = default;

    dtkImageFunctionComposite(dtkImageFunctionComposite&&) = default;
    dtkImageFunctionComposite& operator=(dtkImageFunctionComposite&&) = default;
};

// ///////////////////////////////////////////////////////////////////

class dtkImageFunctionData
{
public:
    virtual ~dtkImageFunctionData(void) = default;

    int value_type(void) const { return m_value_type; }

    virtual void assign_into(xt::xarray<unsigned short>&) const = 0;
    virtual void assign_into(xt::xarray<short>&) const = 0;
    virtual void assign_into(xt::xarray<unsigned char>&) const = 0;
    virtual void assign_into(xt::xarray<char>&) const = 0;
    virtual void assign_into(xt::xarray<unsigned int>&) const = 0;
    virtual void assign_into(xt::xarray<int>&) const = 0;
    virtual void assign_into(xt::xarray<unsigned long>&) const = 0;
    virtual void assign_into(xt::xarray<long>&) const = 0;
    virtual void assign_into(xt::xarray<float>&) const = 0;
    virtual void assign_into(xt::xarray<double>&) const = 0;

    int m_value_type = QMetaType::UnknownType;
};

// ///////////////////////////////////////////////////////////////

template <class Func>
class dtkImageFunctionDataImpl : public dtkImageFunctionData
{
public:
    using function_type = Func;

    dtkImageFunctionDataImpl(const function_type& func);

    void assign_into(xt::xarray<unsigned short>&) const override;
    void assign_into(xt::xarray<short>&) const override;
    void assign_into(xt::xarray<unsigned char>&) const override;
    void assign_into(xt::xarray<char>&) const override;
    void assign_into(xt::xarray<unsigned int>&) const override;
    void assign_into(xt::xarray<int>&) const override;
    void assign_into(xt::xarray<unsigned long>&) const override;
    void assign_into(xt::xarray<long>&) const override;
    void assign_into(xt::xarray<float>&) const override;
    void assign_into(xt::xarray<double>&) const override;

protected:
    function_type m_function;
};

//
// dtkImageFunction.h ends here
