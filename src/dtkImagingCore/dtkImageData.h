// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport>

#include "dtkImagingPixel.h"
#include "dtkImagingTypedef.h"

#include <xtl/xvariant.hpp>

#include <xtensor/xarray.hpp>

#include <QtCore>

// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageData
{
public:
    using variant_type = dtk::imaging::variant_type;
    using shape_type = dtk::imaging::shape_type;
    using PixelType = dtk::imaging::PixelType;

    dtkImageData(void);
    template <typename T>
    dtkImageData(std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, PixelType pixel_type, T *raw_buffer);
    template <typename T>
    dtkImageData(std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, PixelType pixel_type, const T& value);

    ~dtkImageData(void) = default;

    dtkImageData(const dtkImageData&);
    dtkImageData *clone(void) const;

    template <typename T> dtkImageData(std::size_t nx, std::size_t ny, const T = T(0));

    template <typename T> xt::xarray<T>& array(void);
    void  *rawData(void);

    const variant_type& variant(void) const;
          variant_type& variant(void);

    std::size_t dims(void) const;
    int storageType(void) const;
    PixelType pixelType(void) const;

    std::size_t xDim(void) const;
    std::size_t yDim(void) const;
    std::size_t zDim(void) const;

    std::size_t numberOfChannels(void) const;

    std::size_t size(void) const;

public:
     variant_type m_array;
     PixelType m_pixel_type = PixelType::Undefined;
     double origin[3] = {0., 0., 0.};
     double spacing[3] = {0., 0., 0.};
     xt::xarray<double> transform_matrix;
     QVariantHash metadata;
};

// ///////////////////////////////////////////////////////////////////

template <typename T>
inline dtkImageData::dtkImageData(std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, PixelType pixel_type, T *raw_buffer) : m_pixel_type(pixel_type)
{
    Q_ASSERT_X(pixel_type != PixelType::Undefined, Q_FUNC_INFO, "Undefined pixel type.");

    std::size_t c_dim = dtk::imaging::numberOfChannels(pixel_type);
    std::size_t size = x_dim * y_dim * z_dim * c_dim;

    m_array = variant_type(xt::xarray<T>(shape_type({x_dim, y_dim, z_dim, c_dim})));

    xtl::visit(xtl::make_overload([&](auto& a) -> void
                                         {
                                             std::copy(raw_buffer, raw_buffer+size, a.begin()); }), m_array);
}

template <typename T>
inline dtkImageData::dtkImageData(std::size_t x_dim, std::size_t y_dim, std::size_t z_dim, PixelType pixel_type, const T& value) : m_pixel_type(pixel_type)
{
    Q_ASSERT_X(pixel_type != PixelType::Undefined, Q_FUNC_INFO, "Undefined pixel type.");

    std::size_t c_dim = dtk::imaging::numberOfChannels(pixel_type);

    m_array = variant_type(xt::xarray<T>(shape_type({x_dim, y_dim, z_dim, c_dim}), value));

    auto N = this->dims() + 1;
    transform_matrix = xt::eye({N, N});
}

template <typename T>
inline dtkImageData::dtkImageData(std::size_t nx, std::size_t ny, const T val) : m_array(xt::xarray<T>(shape_type({nx, ny, 1, 1}), val)), m_pixel_type(PixelType::Scalar)
{

}

template <typename T>
inline xt::xarray<T>& dtkImageData::array(void)
{
    return xtl::get<xt::xarray<T>>(m_array);
}


//
// dtkImageData.h ends here
