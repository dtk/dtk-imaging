// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport>

class dtkImage;
class dtkImageFunctionBinary;

DTKIMAGINGCORE_EXPORT dtkImageFunctionBinary operator+(const dtkImage& i1, const dtkImage& i2) noexcept;
DTKIMAGINGCORE_EXPORT dtkImageFunctionBinary operator-(const dtkImage& i1, const dtkImage& i2) noexcept;
DTKIMAGINGCORE_EXPORT dtkImageFunctionBinary operator*(const dtkImage& i1, const dtkImage& i2) noexcept;
DTKIMAGINGCORE_EXPORT dtkImageFunctionBinary operator/(const dtkImage& i1, const dtkImage& i2) noexcept;

//
// dtkImageOperation.h ends here
