// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport.h>

#include <dtkCore>

// /////////////////////////////////////////////////////////////////
// dtkImageMeshEngine interface
// /////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageMeshEngine
{
public:
    virtual ~dtkImageMeshEngine(void) {}

public:
    virtual dtkImageMeshEngine *clone(void) const = 0;
};

// /////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkImageMeshEngine *)
DTK_DECLARE_PLUGIN        (dtkImageMeshEngine, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkImageMeshEngine, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkImageMeshEngine, DTKIMAGINGCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DECLARE_CONCEPT(dtkImageMeshEngine, DTKIMAGINGCORE_EXPORT, meshEngine);
}

//
// dtkImageMeshEngine.h ends here
