// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImagingPixel.h"

namespace dtk
{
    namespace imaging
    {
        std::size_t numberOfChannels(PixelType pixel_type) {
            switch(pixel_type) {
            case Scalar:
                return 1;
            case RGB:
                return 3;
            case RGBA:
                return 4;
            default:
                return 0;
            }
        }
    }
}

//
// dtkImagingPixel.cpp ends here
