// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageFunction.h"

dtkImageFunction::~dtkImageFunction(void)
{
    if (d) {
        delete d;
        d = nullptr;
    }
}

dtkImageFunction::dtkImageFunction(dtkImageFunction&& o) : d(o.d)
{
    o.d = nullptr;
}

dtkImageFunction& dtkImageFunction::operator=(dtkImageFunction&& o)
{
    std::swap(d, o.d);
    return *this;
}

int dtkImageFunction::value_type(void) const
{
    return d->value_type();
}

void dtkImageFunction::assign_into(xt::xarray<unsigned short>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

void dtkImageFunction::assign_into(xt::xarray<short>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

void dtkImageFunction::assign_into(xt::xarray<unsigned char>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

void dtkImageFunction::assign_into(xt::xarray<char>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

void dtkImageFunction::assign_into(xt::xarray<unsigned int>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

void dtkImageFunction::assign_into(xt::xarray<int>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

void dtkImageFunction::assign_into(xt::xarray<unsigned long>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

void dtkImageFunction::assign_into(xt::xarray<long>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

void dtkImageFunction::assign_into(xt::xarray<float>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

void dtkImageFunction::assign_into(xt::xarray<double>& a) const
{
    if (d) {
        d->assign_into(a);
    }
}

//
// dtkImageFunction.cpp ends here
