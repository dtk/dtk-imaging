// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport.h>

#include <dtkCore>

#include <QtCore>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractImageStatistics interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkAbstractImageStatistics : public QRunnable
{
public:
    virtual void setImage(dtkImage *image) = 0;

public:
    virtual double     mean(void) const = 0;
    virtual double    sigma(void) const = 0;
    virtual double variance(void) const = 0;
    virtual double      sum(void) const = 0;

    virtual double min(void) const = 0;
    virtual double max(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractImageStatistics *)
DTK_DECLARE_PLUGIN        (dtkAbstractImageStatistics, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractImageStatistics, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractImageStatistics, DTKIMAGINGCORE_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DECLARE_CONCEPT(dtkAbstractImageStatistics, DTKIMAGINGCORE_EXPORT, statistics);
}

//
// dtkAbstractImageStatistics.h ends here
