// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkImageAssign.h"

#include <xtl/xvariant.hpp>

namespace dtk
{

    // ///////////////////////////////////////////////////////////////////

    template <class E>
    inline void image_scalar_add_assign(imaging::variant_type& v, const E& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a += e; }), v);
    }

    template <class E>
    inline void image_scalar_sub_assign(imaging::variant_type& v, const E& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a -= e; }), v);
    }

    template <class E>
    inline void image_scalar_mul_assign(imaging::variant_type& v, const E& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a *= e; }), v);
    }

    template <class E>
    inline void image_scalar_div_assign(imaging::variant_type& v, const E& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a /= e; }), v);
    }

    template <class E>
    inline void image_scalar_modulus_assign(imaging::variant_type& v, const E& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a %= e; },
                                      [](xt::xarray<float>&) {},
                                      [](xt::xarray<double>&) {}),
                   v);
    }

    template <class E>
    inline void image_scalar_bitand_assign(imaging::variant_type& v, const E& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a &= e; },
                                      [](xt::xarray<float>&) {},
                                      [](xt::xarray<double>&) {}),
                   v);
    }

    template <class E>
    inline void image_scalar_bitor_assign(imaging::variant_type& v, const E& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a |= e; },
                                      [](xt::xarray<float>&) {},
                                      [](xt::xarray<double>&) {}),
                   v);
    }

    template <class E>
    inline void image_scalar_bitxor_assign(imaging::variant_type& v, const E& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a ^= e; },
                                      [](xt::xarray<float>&) {},
                                      [](xt::xarray<double>&) {}),
                   v);
    }

    // ///////////////////////////////////////////////////////////////

    template <class E>
    inline void image_assign(imaging::variant_type& v, const xt::xexpression<E>& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a = e; }), v);
    }

    template <class E>
    inline void image_add_assign(imaging::variant_type& v, const xt::xexpression<E>& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a += e; }), v);
    }

    template <class E>
    inline void image_sub_assign(imaging::variant_type& v, const xt::xexpression<E>& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a -= e; }), v);
    }

    template <class E>
    inline void image_mul_assign(imaging::variant_type& v, const xt::xexpression<E>& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a *= e; }), v);
    }

    template <class E>
    inline void image_div_assign(imaging::variant_type& v, const xt::xexpression<E>& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a /= e; }), v);
    }

    template <class E>
    inline void image_modulus_assign(imaging::variant_type& v, const xt::xexpression<E>& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a %= e; },
                                      [](xt::xarray<float>&) {},
                                      [](xt::xarray<double>&) {}),
                   v);
    }

    template <class E>
    inline void image_bitand_assign(imaging::variant_type& v, const xt::xexpression<E>& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a &= e; },
                                      [](xt::xarray<float>&) {},
                                      [](xt::xarray<double>&) {}),
                   v);
    }

    template <class E>
    inline void image_bitor_assign(imaging::variant_type& v, const xt::xexpression<E>& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a |= e; },
                                      [](xt::xarray<float>&) {},
                                      [](xt::xarray<double>&) {}),
                   v);
    }

    template <class E>
    inline void image_bitxor_assign(imaging::variant_type& v, const xt::xexpression<E>& e)
    {
        xtl::visit(xtl::make_overload([&](auto& a) { a ^= e; },
                                      [](xt::xarray<float>&) {},
                                      [](xt::xarray<double>&) {}),
                   v);
    }
}


//
// dtkImageAssign.tpp ends here
