// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include "dtkImageSemantic.h"

#include "dtkImageAssign.h"

// ///////////////////////////////////////////////////////////////////
// dtkImageSemantic implementation
// ///////////////////////////////////////////////////////////////////

template <class D>
inline auto dtkImageSemantic<D>::derived_cast(void) & noexcept -> derived_type&
{
    return *static_cast<derived_type*>(this);
}

template <class D>
inline auto dtkImageSemantic<D>::derived_cast(void) const & noexcept -> const derived_type&
{
    return *static_cast<const derived_type*>(this);
}

template <class D>
inline auto dtkImageSemantic<D>::derived_cast(void) && noexcept -> derived_type
{
    return *static_cast<derived_type*>(this);
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator+=(const E& e) -> dtk::disable_image_function<E, derived_type&>
{
    dtk::image_scalar_add_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator-=(const E& e) -> dtk::disable_image_function<E, derived_type&>
{
    dtk::image_scalar_sub_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator*=(const E& e) -> dtk::disable_image_function<E, derived_type&>
{
    dtk::image_scalar_mul_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator/=(const E& e) -> dtk::disable_image_function<E, derived_type&>
{
    dtk::image_scalar_div_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator%=(const E& e) -> dtk::disable_image_function<E, derived_type&>
{
    dtk::image_scalar_modulus_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator&=(const E& e) -> dtk::disable_image_function<E, derived_type&>
{
    dtk::image_scalar_bitand_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator|=(const E& e) -> dtk::disable_image_function<E, derived_type&>
{
    dtk::image_scalar_bitor_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator^=(const E& e) -> dtk::disable_image_function<E, derived_type&>
{
    dtk::image_scalar_bitxor_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator+=(const xt::xexpression<E>& e) -> derived_type&
{
    dtk::image_add_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator-=(const xt::xexpression<E>& e) -> derived_type&
{
    dtk::image_sub_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator*=(const xt::xexpression<E>& e) -> derived_type&
{
    dtk::image_mul_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator/=(const xt::xexpression<E>& e) -> derived_type&
{
    dtk::image_div_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator%=(const xt::xexpression<E>& e) -> derived_type&
{
    dtk::image_modulus_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator&=(const xt::xexpression<E>& e) -> derived_type&
{
    dtk::image_bitand_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator|=(const xt::xexpression<E>& e) -> derived_type&
{
    dtk::image_bitor_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::operator^=(const xt::xexpression<E>& e) -> derived_type&
{
    dtk::image_bitxor_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
template <class E>
inline auto dtkImageSemantic<D>::assign(const xt::xexpression<E>& e) -> derived_type&
{
    dtk::image_assign(this->derived_cast().variant(), e);
    return this->derived_cast();
}

template <class D>
inline auto dtkImageSemantic<D>::assign(const dtkImageFunction& f) -> derived_type&
{
    dtk::image_function_assign(this->derived_cast().variant(), f);
    return this->derived_cast();
}

//
// dtkImageSemantic.tpp ends here
