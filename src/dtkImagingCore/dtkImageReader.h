// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkCore>

#include <dtkImagingCoreExport.h>

#include <QtCore>

class dtkImage;

// /////////////////////////////////////////////////////////////////
// dtkImageReader interface
// /////////////////////////////////////////////////////////////////

class DTKIMAGINGCORE_EXPORT dtkImageReader
{
public:
    virtual ~dtkImageReader(void) {}

public:
    virtual dtkImage *read(const QString& path) = 0;

public:
    virtual QStringList types(void) = 0;
};

// /////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(        dtkImageReader *)
DTK_DECLARE_PLUGIN(        dtkImageReader, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkImageReader, DTKIMAGINGCORE_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkImageReader, DTKIMAGINGCORE_EXPORT)

typedef dtkCorePluginFactory<dtkImageReader>  dtkImageReaderPluginFactoryTemplate;

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DECLARE_CONCEPT(dtkImageReader, DTKIMAGINGCORE_EXPORT, reader);
}

//
// dtkImageReader.h ends here
