#pragma once

#include "dtkTemplatedPixel.h"

template <typename T> class dtkScalarPixel : public dtkTemplatedPixel< T, typename std::enable_if<std::is_scalar<T>::value>::type>
{
public:

    static const int PixelDim=1;

    static QString pixelId()
    {
        return "Scalar";
    }

    QString identifier() const
    {
        return pixelId();
    }

    int pixelDim() const
    {
        return PixelDim;
    }

};

template <typename T> class dtkRGBPixel : public dtkTemplatedPixel< T, typename std::enable_if<std::is_scalar<T>::value>::type>
{
public:

    static const int PixelDim=3;

    static QString pixelId()
    {
        return "RGB";
    }

    QString identifier() const
    {
        return pixelId();
    }

    int pixelDim() const
    {
        return PixelDim;
    }
};

template <typename T> class dtkRGBAPixel : public dtkTemplatedPixel< T, typename std::enable_if<std::is_scalar<T>::value>::type>
{
public:

    static const int PixelDim=4;

    static QString pixelId()
    {
        return "RGBA";
    }

    QString identifier() const
    {
        return pixelId();
    }

    int pixelDim() const
    {
        return PixelDim;
    }
};
