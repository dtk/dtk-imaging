// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingCoreExport>

#include <cstddef>

namespace dtk
{
    namespace imaging
    {
        enum DTKIMAGINGCORE_EXPORT PixelType {
            Undefined = 0,
               Scalar = 1,
                  RGB = 2,
                 RGBA = 3
        };

        std::size_t DTKIMAGINGCORE_EXPORT numberOfChannels(PixelType);
    }
}

//
// dtkImagingPixel.h ends here
