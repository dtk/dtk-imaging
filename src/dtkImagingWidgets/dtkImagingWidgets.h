// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingWidgetsExport.h>

class dtkImage;

// /////////////////////////////////////////////////////////////////
// Display image
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTKIMAGINGWIDGETS_EXPORT void display(dtkImage *image);
}

//
// dtkImagingWidgets.h ends here
