// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAbstractImageViewer.h"

#include <dtkImagingCore/dtkImaging.h>

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DEFINE_CONCEPT(dtkAbstractImageViewer, viewer, dtkImaging);
}

// /////////////////////////////////////////////////////////////////
// dtkAbstractImageViewer
// /////////////////////////////////////////////////////////////////

/*!
  \class dtkAbstractImageViewer
  \brief The dtkAbstractViewer class is the base class for viewers.

  \ingroup abstractions

  dtkAbstractViewer provides a single pure virtual display() method.
  to reduce the length of the code that has to be written by the user,
  this class does not respect the \l{setInputs run outputs pattern}
*/

//
// dtkAbstractImageViewer.cpp ends here
