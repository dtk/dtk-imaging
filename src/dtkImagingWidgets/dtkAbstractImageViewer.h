// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingWidgetsExport.h>

#include <dtkCore>
#include <dtkWidgetsWidget>

#include <QtCore>

class dtkImage;
class dtkImageMesh;

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class DTKIMAGINGWIDGETS_EXPORT dtkAbstractImageViewer : public dtkWidgetsWidget
{
    Q_OBJECT

public:
    virtual ~dtkAbstractImageViewer(void) {}

public:
    virtual void display(dtkImage *image) = 0;
    virtual void display(dtkImageMesh *mesh) = 0;
};

// /////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractImageViewer *)
DTK_DECLARE_PLUGIN        (dtkAbstractImageViewer, DTKIMAGINGWIDGETS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractImageViewer, DTKIMAGINGWIDGETS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractImageViewer, DTKIMAGINGWIDGETS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    DTK_DECLARE_CONCEPT(dtkAbstractImageViewer, DTKIMAGINGWIDGETS_EXPORT, viewer);
}

//
// dtkAbstractImageViewer.h ends here
