// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <QtCore>

#include <dtkCore>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractErosionFilter interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractErosionFilter : public QRunnable
{
public:
    virtual void run(void) = 0;

public:
    virtual void setImage(dtkImage *image) = 0;
    virtual void setRadius(int radius) = 0;
    virtual void setIterationCount(int iteration_count) = 0;

public:
    virtual dtkImage *filteredImage(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkAbstractErosionFilter *)
DTK_DECLARE_PLUGIN(dtkAbstractErosionFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractErosionFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractErosionFilter, DTKIMAGINGFILTERS_EXPORT)

// ///////////////////////////////////////////////////////////////////
// Register dtkAbstractErosionFilter to the layers
// ///////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractErosionFilter, DTKIMAGINGFILTERS_EXPORT, erosion);
    }
}

//
// dtkAbstractErosionFilter.h ends here
