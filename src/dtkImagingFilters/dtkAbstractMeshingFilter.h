// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;
class dtkImageMesh;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractMeshingFilteringProcess process interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractMeshingFilter : public QRunnable
{
public:
    virtual void setInput(dtkImage *image) = 0;

public:
    virtual dtkImageMesh *result(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractMeshingFilter *)
DTK_DECLARE_PLUGIN        (dtkAbstractMeshingFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractMeshingFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractMeshingFilter, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractMeshingFilter, DTKIMAGINGFILTERS_EXPORT, meshing);
    }
}

//
// dtkAbstractMeshingFilter.h ends here
