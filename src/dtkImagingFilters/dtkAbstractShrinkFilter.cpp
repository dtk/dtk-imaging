// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAbstractShrinkFilter.h"

#include "dtkImaging.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DEFINE_CONCEPT(dtkAbstractShrinkFilter, shrink, dtkImaging);
    }
}

//
// dtkAbstractShrinkFilter.cpp ends here
