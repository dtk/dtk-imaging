// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include<QRunnable>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractThresholdFilter process interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractThresholdFilter : public QRunnable
{
public:
    virtual void setThreshold(double threshold) = 0;
    virtual void setOutsideValue(double outsideValue) = 0;
    virtual void setMode(bool mode) = 0;
    virtual void setImage(dtkImage *image) = 0;

public:
    virtual dtkImage *filteredImage(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractThresholdFilter *)
DTK_DECLARE_PLUGIN        (dtkAbstractThresholdFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractThresholdFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractThresholdFilter, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractThresholdFilter, DTKIMAGINGFILTERS_EXPORT, threshold);
    }
}

//
// dtkAbstractThresholdFilter.h ends here
