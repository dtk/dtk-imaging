// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <QtCore>

#include <dtkCore>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractHTransformFilter : public QRunnable
{
public:
    virtual void run(void) = 0;

public:
    virtual void setImage(dtkImage *image) = 0;
    virtual void setHValue(int value) = 0;

public:
    virtual dtkImage *filteredImage(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkAbstractHTransformFilter *)
DTK_DECLARE_PLUGIN(dtkAbstractHTransformFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractHTransformFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractHTransformFilter, DTKIMAGINGFILTERS_EXPORT)

// ///////////////////////////////////////////////////////////////////
// Register dtkAbstractHTransformFilter to the layers
// ///////////////////////////////////////////////////////////////////

namespace dtkImaging {

    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractHTransformFilter, DTKIMAGINGFILTERS_EXPORT, htransform);
    }
}

//
// dtkAbstractHTransformFilter.h ends here
