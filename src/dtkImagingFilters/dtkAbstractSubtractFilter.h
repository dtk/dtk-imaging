// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractSubstractFiltering interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractSubtractFilter : public QRunnable
{
public:
    virtual void setValue(double value) = 0;
    virtual void setImage(dtkImage *image) = 0;

public:
    virtual dtkImage *filteredImage(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractSubtractFilter *)
DTK_DECLARE_PLUGIN        (dtkAbstractSubtractFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractSubtractFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractSubtractFilter, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractSubtractFilter, DTKIMAGINGFILTERS_EXPORT, subtract);
    }
}

//
// dtkAbstractSubtractFilter.h ends here
