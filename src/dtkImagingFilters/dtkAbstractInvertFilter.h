// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractInvertFiltering interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractInvertFilter : public QRunnable
{
public:
    virtual void setImage(dtkImage *image) = 0;

public:
    virtual dtkImage *filteredImage(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractInvertFilter *)
DTK_DECLARE_PLUGIN        (dtkAbstractInvertFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractInvertFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractInvertFilter, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractInvertFilter, DTKIMAGINGFILTERS_EXPORT, invert);
    }
}

//
// dtkAbstractInvertFilter.h ends here
