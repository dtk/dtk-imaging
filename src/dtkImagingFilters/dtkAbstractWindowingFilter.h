// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractWindowingFiltering interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractWindowingFilter : public QRunnable
{
public:
    virtual void setMinimumIntensityValue(double value) = 0;
    virtual void setMaximumIntensityValue(double value) = 0;
    virtual void setMinimumOutputIntensityValue(double value) = 0;
    virtual void setMaximumOutputIntensityValue(double value) = 0;

    virtual void setImage(dtkImage *image) = 0;

public:
    virtual dtkImage *filteredImage(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractWindowingFilter *)
DTK_DECLARE_PLUGIN        (dtkAbstractWindowingFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractWindowingFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractWindowingFilter, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractWindowingFilter, DTKIMAGINGFILTERS_EXPORT, windowing);
    }
}

//
// dtkAbstractWindowingFilter.h ends here
