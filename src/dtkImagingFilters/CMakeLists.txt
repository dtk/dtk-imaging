## Version: $Id$
##
######################################################################
##
### Commentary:
##
######################################################################
##
### Change Log:
##
######################################################################
##
### Code:

project(dtkImagingFilters)

## ###################################################################
## Input
## ###################################################################

set(${PROJECT_NAME}_HEADERS
  dtkAbstractAddFilter.h
  dtkAbstractCloseFilter.h
  dtkAbstractConnectedComponentsFilter.h
  dtkAbstractConnectivityThreshold.h
  dtkAbstractDilateFilter.h
  dtkAbstractDistanceMapFilter.h
  dtkAbstractDivideFilter.h
  dtkAbstractErodeFilter.h
  dtkAbstractErosionFilter
  dtkAbstractErosionFilter.h
  dtkAbstractGaussianFilter.h
  dtkAbstractHTransformFilter.h
  dtkAbstractImageMeshMapping.h
  dtkAbstractImageSubtraction.h
  dtkAbstractInvertFilter.h
  dtkAbstractMaskApplicationFilter.h
  dtkAbstractMedianFilter.h
  dtkAbstractMeshICP.h
  dtkAbstractMeshingFilter.h
  dtkAbstractMultiplyFilter.h
  dtkAbstractNormalizeFilter.h
  dtkAbstractOpenFilter.h
  dtkAbstractShrinkFilter.h
  dtkAbstractSubtractFilter.h
  dtkAbstractSegmentation.h
  dtkAbstractThresholdFilter.h
  dtkAbstractWatershedFilter
  dtkAbstractWatershedFilter.h
  dtkAbstractWindowingFilter.h
  dtkAbstractXorFilter.h)

set(${PROJECT_NAME}_SOURCES
  dtkAbstractAddFilter.cpp
  dtkAbstractCloseFilter.cpp
  dtkAbstractConnectedComponentsFilter.cpp
  dtkAbstractConnectivityThreshold.cpp
  dtkAbstractDilateFilter.cpp
  dtkAbstractDistanceMapFilter.cpp
  dtkAbstractDivideFilter.cpp
  dtkAbstractErodeFilter.cpp
  dtkAbstractErosionFilter.cpp
  dtkAbstractGaussianFilter.cpp
  dtkAbstractHTransformFilter.cpp
  dtkAbstractImageMeshMapping.cpp
  dtkAbstractImageSubtraction.cpp
  dtkAbstractInvertFilter.cpp
  dtkAbstractMaskApplicationFilter.cpp
  dtkAbstractMedianFilter.cpp
  dtkAbstractMeshICP.cpp
  dtkAbstractMeshingFilter.cpp
  dtkAbstractMultiplyFilter.cpp
  dtkAbstractNormalizeFilter.cpp
  dtkAbstractOpenFilter.cpp
  dtkAbstractShrinkFilter.cpp
  dtkAbstractSubtractFilter.cpp
  dtkAbstractSegmentation.cpp
  dtkAbstractThresholdFilter.cpp
  dtkAbstractWatershedFilter.cpp
  dtkAbstractWindowingFilter.cpp
  dtkAbstractXorFilter.cpp)

## ###################################################################
## Build rules
## ###################################################################

qt5_add_RESOURCES(${PROJECT_NAME}_SOURCES_QRC ${${PROJECT_NAME}_RESOURCES})

add_library(${PROJECT_NAME} SHARED
  ${${PROJECT_NAME}_SOURCES}
  ${${PROJECT_NAME}_HEADERS})

set_target_properties(${PROJECT_NAME} PROPERTIES MACOSX_RPATH 0)
set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_NAME_DIR "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
set_target_properties(${PROJECT_NAME} PROPERTIES INSTALL_RPATH    "${CMAKE_INSTALL_PREFIX}/${CMAKE_INSTALL_LIBDIR}")
set_target_properties(${PROJECT_NAME} PROPERTIES VERSION   ${dtkImaging_VERSION}
                                                 SOVERSION ${dtkImaging_VERSION_MAJOR})

## ###################################################################
## Link rules
## ###################################################################

target_link_libraries(${PROJECT_NAME} PUBLIC Qt5::Core)
target_link_libraries(${PROJECT_NAME} PUBLIC dtkCore)
target_link_libraries(${PROJECT_NAME} PRIVATE dtkLog)
target_link_libraries(${PROJECT_NAME} PUBLIC dtkImagingCore)

target_include_directories(${PROJECT_NAME} PUBLIC
  $<BUILD_INTERFACE:${layer_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_CURRENT_SOURCE_DIR}>
  $<BUILD_INTERFACE:${CMAKE_BINARY_DIR}>
  $<BUILD_INTERFACE:${PROJECT_BINARY_DIR}>
  $<BUILD_INTERFACE:${layer_BINARY_DIR}>
  $<INSTALL_INTERFACE:include/${PROJECT_NAME}>
  $<INSTALL_INTERFACE:include>)

## ###################################################################
## Export header file
## ###################################################################

generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export.h")
generate_export_header(${PROJECT_NAME} EXPORT_FILE_NAME "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export")

set(${PROJECT_NAME}_HEADERS
  ${${PROJECT_NAME}_HEADERS}
  "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export"
  "${CMAKE_BINARY_DIR}/${PROJECT_NAME}Export.h")

## ###################################################################
## Install rules - files
## ###################################################################

install(FILES ${${PROJECT_NAME}_HEADERS}
  DESTINATION include/${PROJECT_NAME}
)

## ###################################################################
## Install rules - targets
## ###################################################################

install(TARGETS ${PROJECT_NAME} EXPORT layer-targets
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
  RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR})

######################################################################
### CMakeLists.txt ends here
