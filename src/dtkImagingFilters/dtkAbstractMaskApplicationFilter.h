// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractMaskApplicationFiltering interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractMaskApplicationFilter : public QRunnable
{
public:
    virtual void setImage(dtkImage *image) = 0;
    virtual void setMask(dtkImage *image) = 0;
    virtual void setBackgroundValue(double) = 0;

public:
    virtual dtkImage *result(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractMaskApplicationFilter *)
DTK_DECLARE_PLUGIN        (dtkAbstractMaskApplicationFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractMaskApplicationFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractMaskApplicationFilter, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractMaskApplicationFilter, DTKIMAGINGFILTERS_EXPORT, maskApplication);
    }
}

//
// dtkAbstractMaskApplicationFilter.h ends here
