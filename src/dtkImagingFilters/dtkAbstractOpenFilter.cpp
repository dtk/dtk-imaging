// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAbstractOpenFilter.h"

#include "dtkImaging.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DEFINE_CONCEPT(dtkAbstractOpenFilter, open, dtkImaging);
    }
}

//
// dtkAbstractOpenFilter.cpp ends here
