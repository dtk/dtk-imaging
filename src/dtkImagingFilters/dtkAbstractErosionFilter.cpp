// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkAbstractErosionFilter.h"

#include "dtkImaging.h"

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DEFINE_CONCEPT(dtkAbstractErosionFilter, erosion, dtkImaging);
    }
}

//
// dtkAbstractErosionFilter.cpp ends here
