// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:
#pragma once
#include <dtkImagingFiltersExport.h>

#include <QtCore>
#include <dtkCore>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
//
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractWatershedFilter : public QRunnable
{
 public:
    virtual void setImage(dtkImage *image) = 0;
    virtual void setSeed(dtkImage *seed) = 0;

    virtual void setControlParameter(const QString& control_parameter) = 0;

 public:
    virtual dtkImage *filteredImage(void) const = 0;

 public:
    virtual void run(void) = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT(dtkAbstractWatershedFilter *)
DTK_DECLARE_PLUGIN(dtkAbstractWatershedFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractWatershedFilter, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractWatershedFilter, DTKIMAGINGFILTERS_EXPORT)

// ///////////////////////////////////////////////////////////////////
// Register dtkAbstractWatershedFilter to the layers
// ///////////////////////////////////////////////////////////////////

namespace dtkImaging {

    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractWatershedFilter, DTKIMAGINGFILTERS_EXPORT, watershed);
    }
}

//
// dtkAbstractWatershedFilter.h ends here
