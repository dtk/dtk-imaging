// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <dtkImagingFiltersExport.h>

#include <dtkCore>

#include <QRunnable>

class dtkImage;

// ///////////////////////////////////////////////////////////////////
// dtkAbstractSubstractFiltering interface
// ///////////////////////////////////////////////////////////////////

class DTKIMAGINGFILTERS_EXPORT dtkAbstractSegmentation : public QRunnable
{
public:
    virtual void setImage(dtkImage *image) = 0;

    virtual void setThreshold(const double) = 0;
    virtual void setLevel(const double) = 0;

public:
    virtual dtkImage *segmentedImage(void) const = 0;
};

// ///////////////////////////////////////////////////////////////////

DTK_DECLARE_OBJECT        (dtkAbstractSegmentation *)
DTK_DECLARE_PLUGIN        (dtkAbstractSegmentation, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_FACTORY(dtkAbstractSegmentation, DTKIMAGINGFILTERS_EXPORT)
DTK_DECLARE_PLUGIN_MANAGER(dtkAbstractSegmentation, DTKIMAGINGFILTERS_EXPORT)

// /////////////////////////////////////////////////////////////////
// Register to dtkImaging layer
// /////////////////////////////////////////////////////////////////

namespace dtkImaging {
    namespace filters {
        DTK_DECLARE_CONCEPT(dtkAbstractSegmentation, DTKIMAGINGFILTERS_EXPORT, segmentation);
    }
}

//
// dtkAbstractSegmentation.h ends here
