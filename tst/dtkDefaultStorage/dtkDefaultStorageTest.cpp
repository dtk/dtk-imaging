// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkDefaultStorageTest.h"
#include "dtkImageDefaultStorage.h"
#include "dtkPixelImpl.h"

#include <dtkImagingCore>

void dtkDefaultStorageTestCase::initTestCase(void)
{

}

void dtkDefaultStorageTestCase::init(void)
{

}

void dtkDefaultStorageTestCase::testRawData(void)
{
    double *values = new double[25];

    for(int i = 0; i < 25 ;i++)
        values[i] = i;

    dtkImageDefaultStorage<dtkScalarPixel<double>, 2> *storage = new dtkImageDefaultStorage<dtkScalarPixel<double>, 2>(5, 5, values);

    double *data = static_cast<double*>(storage->rawData());

    for(int i = 0; i < 25;i++)
        QVERIFY(data[i] == i);

    delete storage;
}

void dtkDefaultStorageTestCase::testIdentifier(void)
{
    dtkImage *img=new dtkImage(5,5);

    QVERIFY(img->identifier() == "default");
}

void dtkDefaultStorageTestCase::cleanupTestCase(void)
{

}

void dtkDefaultStorageTestCase::cleanup(void)
{

}

DTKTEST_MAIN_NOGUI(dtkDefaultStorageTest, dtkDefaultStorageTestCase)

//
// dtkDefaultStorageTest.cpp ends here
