// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>


class dtkDefaultStorageTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testRawData(void);
    void testIdentifier(void);


private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

// 
// dtkDefaultStorageTest.h ends here
