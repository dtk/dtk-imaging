// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>


class dtkImageTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testCopy(void);
    void testView(void);


private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

// 
// dtkImageTest.h ends here
