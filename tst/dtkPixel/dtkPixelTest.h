// Version: $Id$
// 
// 

// Commentary: 
// 
// 

// Change Log:
// 
// 

// Code:

#pragma once

#include <dtkTest>


class dtkPixelTestCase : public QObject
{
    Q_OBJECT

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testScalarPixel(void);
    void testRGBPixel(void);
    void testCompileFailure(void);


private slots:
    void cleanupTestCase(void);
    void cleanup(void);
};

// 
// dtkPixelTest.h ends here
