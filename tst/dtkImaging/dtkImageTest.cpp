// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include "dtkImageTest.h"

#include <dtkImagingTest>

#include <dtkImagingCore>

#include <xtensor/xarray.hpp>
#include <xtensor/xio.hpp>
#include <xtensor/xrandom.hpp>
#include <xtensor/xbuilder.hpp>

#include <xtl/xvariant.hpp>

using PixelType = dtk::imaging::PixelType;

// ///////////////////////////////////////////////////////////////////
// dtkImageTestCasePrivate declaration
// ///////////////////////////////////////////////////////////////////

class dtkImageTestCasePrivate
{
public:
    std::size_t xdim = 9;
    std::size_t ydim = 7;
    std::size_t zdim = 5;
};

// ///////////////////////////////////////////////////////////////////

dtkImageTestCase::dtkImageTestCase(void) : d(new dtkImageTestCasePrivate)
{
}

dtkImageTestCase::~dtkImageTestCase(void)
{
    delete d;
}

void dtkImageTestCase::initTestCase(void)
{

}

void dtkImageTestCase::init(void)
{
}

void dtkImageTestCase::testStorageType(void)
{
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, (unsigned short)(41));
        QVERIFY(i.storageType() == QMetaType::UShort);
    }
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, (short)(41));
        QVERIFY(i.storageType() == QMetaType::Short);
    }
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, (unsigned char)(41));
        QVERIFY(i.storageType() == QMetaType::UChar);
    }
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, (char)(41));
        QVERIFY(i.storageType() == QMetaType::Char);
    }
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, (unsigned int)(41));
        QVERIFY(i.storageType() == QMetaType::UInt);
    }
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, (int)(41));
        QVERIFY(i.storageType() == QMetaType::Int);
    }
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, (unsigned long)(41));
        QVERIFY(i.storageType() == QMetaType::ULong);
    }
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, (long)(41));
        QVERIFY(i.storageType() == QMetaType::Long);
    }
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, std::sqrt(2.));
        QVERIFY(i.storageType() == QMetaType::Double);
    }
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, (float)(2.));
        QVERIFY(i.storageType() == QMetaType::Float);
    }
}

void dtkImageTestCase::testDimAndSize(void)
{
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, std::sqrt(2.));
        QVERIFY(i.dims() == 0);
        QVERIFY(i.xDim() == 1);
        QVERIFY(i.yDim() == 1);
        QVERIFY(i.zDim() == 1);
        QVERIFY(i.size() == 1);
    }

    {
        dtkImage i(d->xdim, 1, 1, PixelType::Scalar, std::sqrt(2.));
        QVERIFY(i.dims() == 1);
        QVERIFY(i.xDim() == d->xdim);
        QVERIFY(i.yDim() == 1);
        QVERIFY(i.zDim() == 1);
        QVERIFY(i.size() == d->xdim);
    }

    {
        dtkImage i(d->xdim, d->ydim, 1, PixelType::Scalar, std::sqrt(2.));
        QVERIFY(i.dims() == 2);
        QVERIFY(i.xDim() == d->xdim);
        QVERIFY(i.yDim() == d->ydim);
        QVERIFY(i.zDim() == 1);
        QVERIFY(i.size() == d->xdim * d->ydim);
    }

    {
        dtkImage i(d->xdim, 1, d->zdim, PixelType::Scalar, std::sqrt(2.));
        QVERIFY(i.dims() == 2);
        QVERIFY(i.xDim() == d->xdim);
        QVERIFY(i.yDim() == 1);
        QVERIFY(i.zDim() == d->zdim);
        QVERIFY(i.size() == d->xdim * d->zdim);
    }

    {
        dtkImage i(d->xdim, d->ydim, d->zdim, PixelType::Scalar, std::sqrt(2.));
        QVERIFY(i.dims() == 3);
        QVERIFY(i.xDim() == d->xdim);
        QVERIFY(i.yDim() == d->ydim);
        QVERIFY(i.zDim() == d->zdim);
        QVERIFY(i.size() == d->xdim * d->zdim * d->ydim);
    }
}

void dtkImageTestCase::testPixelTypeAndNumberOfChannels(void)
{
    {
        dtkImage i(1, 1, 1, PixelType::Scalar, std::sqrt(2.));
        QVERIFY(i.pixelType() == PixelType::Scalar);
        QVERIFY(i.numberOfChannels() == 1);
    }
    {
        dtkImage i(1, 1, 1, PixelType::RGB, std::sqrt(2.));
        QVERIFY(i.pixelType() == PixelType::RGB);
        QVERIFY(i.numberOfChannels() == 3);
    }
    {
        dtkImage i(1, 1, 1, PixelType::RGBA, std::sqrt(2.));
        QVERIFY(i.pixelType() == PixelType::RGBA);
        QVERIFY(i.numberOfChannels() == 4);
    }
}

void dtkImageTestCase::testOrigin(void)
{
    {
        dtkImage i(d->xdim, d->ydim, d->zdim, PixelType::Scalar, std::sqrt(2.));
        double o[3] = {-4.5, 112., 0.002568};
        i.setOrigin(o);
        auto&& ori = i.origin();
        for (int i = 0; i < 3; ++i) {
            QCOMPARE(ori[i], o[i]);
        }
    }
}

void dtkImageTestCase::testSpacing(void)
{
    {
        dtkImage i(d->xdim, d->ydim, d->zdim, PixelType::Scalar, std::sqrt(2.));
        double s[3] = {0.5, 0.7, 1.6};
        i.setSpacing(s);
        auto&& spc = i.spacing();
        for (int i = 0; i < 3; ++i) {
            QCOMPARE(spc[i], s[i]);
        }
    }
}

void dtkImageTestCase::testTransformationMatrix(void)
{
    using shape_type = dtk::imaging::shape_type;
    {
        dtkImage i(d->xdim, d->ydim, d->zdim, PixelType::Scalar, std::sqrt(2.));

        xt::xarray<double> tm1 = xt::ones<double>(shape_type({i.dims()+1, i.dims()+1}));

        i.setTransformMatrix(tm1.data());

        auto tm2 = i.transformMatrix();

        QVERIFY(tm2 == tm1);
    }
}

void dtkImageTestCase::testRawData(void)
{
    using shape_type = dtk::imaging::shape_type;
    {
        std::size_t size = d->xdim*d->ydim*d->zdim;
        double *data = new double[size];
        for (int i = 0; i < size; ++i) {
            data[i] = i * std::sqrt(2.);
        }

        dtkImage img(d->xdim, d->ydim, d->zdim, PixelType::Scalar, data);

        double *i_data = reinterpret_cast<double *>(img.rawData());
        QVERIFY(i_data);
        for (int i = 0; i < size; ++i) {
            QCOMPARE(i_data[i], data[i]);
        }

        dtkImage img2(d->xdim, d->ydim, d->zdim, PixelType::Scalar, std::sqrt(2.));
        i_data = reinterpret_cast<double *>(img2.rawData());
        QVERIFY(i_data);
        for (int i = 0; i < size; ++i) {
            QCOMPARE(i_data[i], std::sqrt(2.));
        }
    }
}

void dtkImageTestCase::testBasic(void)
{
    dtkImage i;

    {
        i += 9;
        dtkImage o(i);
        std::cout << xtl::get<0>(i.variant()) << std::endl;

        // using shape_type = xt::xarray<unsigned short>::shape_type;
        // xt::xarray<unsigned short> xa = xt::eye({3, 3});
        // xt::xarray<unsigned short> xb = xt::eye({3, 3}, 1);

        // i += xa - 2*xb;

        // std::cout << xtl::get<0>(i.variant()) << std::endl;
    }

    {
        i -= 3;
        dtkImage o(i);
        std::cout << xtl::get<0>(i.variant()) << std::endl;
    }

    {
        i *= 4;
        dtkImage o(i);
        std::cout << xtl::get<0>(i.variant()) << std::endl;
    }

    {
        i /= 2;
        dtkImage o(i);
        std::cout << xtl::get<0>(i.variant()) << std::endl;
    }

    {
        i %= 3;
        dtkImage o(i);
        std::cout << xtl::get<0>(i.variant()) << std::endl;
    }

    {
        i &= 2;
        dtkImage o(i);
        std::cout << xtl::get<0>(i.variant()) << std::endl;
    }

    {
        i |= 1;
        dtkImage o(i);
        std::cout << xtl::get<0>(i.variant()) << std::endl;
    }

    {
        i ^= 0;
        dtkImage o(i);
        std::cout << xtl::get<0>(i.variant()) << std::endl;
    }

    {
        dtkImage o1 = xt::eye({3, 3});
        dtkImage o2 = xt::eye({3, 3}, 1);
        auto&& e = o1 + o2;
        std::cout << "i: \n" << xtl::get<0>(o1.variant()) << std::endl;
        std::cout << "o: \n" << xtl::get<0>(o2.variant()) << std::endl;

        dtkImage u;
        std::cout << "u: \n" << xtl::get<0>(u.variant()) << std::endl;
        u = e;
        std::cout << "u: \n" << xtl::get<0>(u.variant()) << std::endl;
    }

    // {
    //     dtkImage o;
    //     auto&& e = i - o;
    //     std::cout << "i: " << xtl::get<0>(i.variant()) << std::endl;
    //     std::cout << "o: " << xtl::get<0>(o.variant()) << std::endl;

    //     dtkImage u;
    //     std::cout << "u: " << xtl::get<0>(u.variant()) << std::endl;
    //     u = e;
    //     std::cout << "u: " << xtl::get<0>(u.variant()) << std::endl;
    // }

    {
        using shape_type = xt::xarray<unsigned short>::shape_type;
        xt::xarray<unsigned short> xa = xt::eye({4, 4});
        xt::xarray<unsigned short> xb = xt::eye({4, 4}, 1);
        xt::xarray<unsigned short> xc = xt::eye({4, 4}, 2);

        dtkImage a = xa + xb - xc;
        auto var = a.variant();
        std::cout << "a: \n" << xtl::get<xt::xarray<unsigned short>>(var) << std::endl;

        xt::xarray<unsigned short> xd = xa + xb - xc;
        std::cout << "xd: \n" << xd << std::endl;

        a = xa;
        dtkImage b = xb - xc;

        dtkImage c;

        auto&& e = a + b;
        auto&& f = a - b;
        auto&& g = a * b;

        c = e;
        std::cout << "c: \n" << xtl::get<xt::xarray<unsigned short>>(c.variant()) << std::endl;
        c = f;
        std::cout << "c: \n" << xtl::get<xt::xarray<unsigned short>>(c.variant()) << std::endl;
        c = g;
        std::cout << "c: \n" << xtl::get<xt::xarray<unsigned short>>(c.variant()) << std::endl;

    }
    //     a += xb;
    //     dtkImage o(a);

    //     dtkImage oo(xa+xb);

    //     a -= xc;
    //     dtkImage ooo(a);

    //     dtkImage oooo((xa+xb) - xc);
    // }

}

void dtkImageTestCase::testCopy(void) {
    dtkImage i(1, 1, 1, PixelType::Scalar, std::sqrt(2.));

    double s[3] = {0.5, 0.7, 1.6};
    i.setSpacing(s);
    double o[3] = {10.0, 11.0, 12.0};
    i.setOrigin(o);

    dtkImage j = dtkImage(i);
    QCOMPARE(i.xDim(), j.xDim());
    QCOMPARE(i.yDim(), j.yDim());
    QCOMPARE(i.zDim(), j.zDim());
    const double *s_j = new double[3];
    const double *o_j = new double[3];
    o_j = j.origin();
    s_j = j.spacing();
    for (int k = 0; k < 3; ++k) {
        QCOMPARE(o[k], o_j[k]);
        QCOMPARE(s[k], s_j[k]);
    }
    auto tm  = i.transformMatrix();
    auto tm2 = j.transformMatrix();
    QVERIFY(tm == tm2);
}

void dtkImageTestCase::cleanupTestCase(void)
{

}

void dtkImageTestCase::cleanup(void)
{
}

DTKIMAGINGTEST_MAIN_NOGUI(dtkImageTest, dtkImageTestCase);

//
// dtkImageTest.cpp ends here
