// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkImageTestCase : public QObject
{
    Q_OBJECT

public:
     dtkImageTestCase(void);
    ~dtkImageTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void testStorageType(void);
    void testDimAndSize(void);
    void testPixelTypeAndNumberOfChannels(void);
    void testOrigin(void);
    void testSpacing(void);
    void testTransformationMatrix(void);
    void testBasic(void);
    void testRawData(void);
    void testCopy(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class dtkImageTestCasePrivate *d;
};

//
// dtkImageTest.h ends here
