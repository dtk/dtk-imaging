/*!
    \page dtkImaging-advanced-features
    \title Advanced features
    \ingroup HowTo
    
    \section1 Platform overview

    This API is designed for advanced users willing to extend the
    functionalities of the layer.

    \section2 Data

    The data API is the base of the imaging layer, everything else is
    built on top of it. It provides a default templated storage class
    for images.

    \section2 Images

    To achieve the aforementioned behavior, the dtkImage class is
    actually only a façade over the dtkImageData class, responsible
    for storing image data. This class is pure virtual.

    dtkTemplatedImageData class subclasses dtkImageData and is
    templated over a pixel type and an image dimension. It is still
    abstract but it provides a few methods.

    A concrete image implementation class is provided by the
    dtkDefaultImageStorage, templated over a Pixel type and an image
    dimension. Plugins can provide their own image types, but are
    assumed to fit the dtkImage-based API anyway.

    \section2 Pixels

    Pixels type uses the same hierarchy as described above: dtkPixel
    is a pure virtual class, non-templated to allow user to use their
    own pixel types without constraints if need be. dtkTemplatedPixel
    class derives from it and defines the storage type and the pixel
    dimension. This class is abstract too. Concrete implementations
    define the pixel semantics, for example dtkRGBPixel.
    
    */
