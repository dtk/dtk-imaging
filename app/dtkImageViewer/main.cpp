// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtWidgets>

#include <dtkLog>
#include <dtkCore>

#include <dtkImagingCore>
#include <dtkImagingWidgets>

int main(int argc, char* argv[])
{
    dtkApplication application(argc, argv);
    application.setApplicationName("dtkImageViewer");
    application.setOrganizationName("inria");
    application.setOrganizationDomain("fr");
    application.setApplicationVersion("1.1.0");

    QCommandLineParser& parser = *(application.parser());
    parser.setApplicationDescription("DTK Image Viewer, Args: <image>");

    application.initialize();

    QCommandLineOption verboseOption("verbose", QCoreApplication::translate("main", "verbose plugin initialization"));

    if (parser.isSet(verboseOption)) {
        dtkImaging::setVerboseLoading(true);
    }
    const QStringList positionalArguments = parser.positionalArguments();

    if (positionalArguments.isEmpty()) {
        dtkWarn() << "no image to display";
        return 0;
    }

    dtkImagingSettings settings;
    settings.beginGroup("imaging");
    dtkImaging::reader::pluginManager().initialize(settings.value("plugins").toString());
    dtkImaging::viewer::pluginManager().initialize(settings.value("plugins").toString());
    settings.endGroup();

    QString sourceDataPath = positionalArguments.at(0);

    dtkImage *image = dtkImaging::read(sourceDataPath);

    dtkImaging::display(image);

    return application.exec();
}

//
// main.cpp ends here
