// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <QtCore>

#include <dtkCore>

#include <dtkImagingCore>

int main(int argc, char **argv)
{
    dtkCoreApplication app(argc,argv);
    app.setApplicationName("dtkImageStatistics");
    app.setOrganizationName("inria");
    app.setOrganizationDomain("fr");
    app.setApplicationVersion("1.1.0");

    QCommandLineParser& parser = *(app.parser());
    parser.setApplicationDescription("DTK imaging statistics.");

    app.initialize();

    QCommandLineOption verboseOption("verbose", QCoreApplication::translate("main", "verbose plugin initialization"));

    if (parser.isSet(verboseOption)) {
        dtkImaging::setVerboseLoading(true);
    }

    const QStringList positionalArguments = parser.positionalArguments();
    if(positionalArguments.isEmpty())
    {
        dtkWarn()<<"no image to analyze";
        return -2;
    }

    dtkImagingSettings settings;
    settings.beginGroup("imaging");
    dtkImaging::reader::pluginManager().initialize(settings.value("plugins").toString());
    dtkImaging::statistics::pluginManager().initialize(settings.value("plugins").toString());
    settings.endGroup();

    dtkImage *img = dtkImaging::read(positionalArguments.at(0));

    QStringList statsKeys = dtkImaging::statistics::pluginFactory().keys();

    if(statsKeys.isEmpty())
        return -1;

    dtkAbstractImageStatistics *statsPlugin = dtkImaging::statistics::pluginFactory().create(statsKeys.at(0));
    statsPlugin->setImage(img);
    statsPlugin->run();

    double mean, sigma, var, sum, min, max;

    mean = statsPlugin->mean();
    sigma = statsPlugin->sigma();
    var = statsPlugin->variance();
    sum = statsPlugin->sum();

    min = statsPlugin->min();
    max = statsPlugin->max();

    qDebug()<<"dimensions: "<<img->extent();
    qDebug()<<"origin (physical): "<<img->origin();
    qDebug()<<"spacing (physical): "<<img->spacing();

    qDebug()<<"mean: "<<mean;
    qDebug()<<"sigma: "<<sigma;
    qDebug()<<"var: "<<var;
    qDebug()<<"sum: "<<sum;

    qDebug()<<"min: "<<min;
    qDebug()<<"max: "<<max;

    return 0;
}

//
// main.cpp ends here
