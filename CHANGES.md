# ChangLog
## version 2.0.4 - 2019-09-25
 * fix copy constructor of dtkImage
 * use conda prefix as default search path for plugins
## version 2.0.3 - 2019-09-24
 * add fromRawData static method in dtkImage to ease conversions
## version 2.0.2 - 2019-09-20
 * windows: fix export
 * add dtkImage to Qt metatype system
 * change default install path when running in conda env
## version 2.0.1 - 2019-09-20
 * fix install of dtkImagingWidgets + export cmake variable for DTKIMAGING_ENABLE_WIDGETS
## version 2.0.0 - 2019-09-19
 * switch to dtk 2
 * disable composer layer for the moment
## version 1.2.0 - 2018-11-09
 * CMake version required 3.6
 * C++14 compilation
 * Abstract Segmentation modified to set level and threshold
## version 1.1.1 - 2018-11-09
 * fix missing destruction of the data in dtkImage
## version 1.1.0 - 2018-07-25
 * use DTK_PYTHON_INSTALL_PATH
 * add setAutoLoading in dtkImaging namespace
## version 1.0.5 - 2018-07-23
 * fix for conda and DTK_INSTALL_PREFIX
## version 1.0.4 - 2018-07-20
 * clean path when using DTK_INSTALL_PREFIX
## version 1.0.3 - 2018-03-14
 * fix bad install of dtkImagingWidgets headers
## version 1.0.2 - 2018-03-14
 * really fix INSTALL_RPATH for composer extension
## version 1.0.1 - 2018-03-09
- fix target properties for composer extension plugin
## version 1.0.0 - 2018-03-08
- add filters abstractions (ErosionFilter, WatershedFilter, HTransform)
- add python wrapping
- layer rearchitecturing: split into Filter, Core, Composer and Widgets components
